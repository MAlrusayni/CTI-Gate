# CTI Gate
This is my college project, it is a website for my college. I use 
[Rust](https://www.rust-lang.org/) and [Rocket](https://rocket.rs/) to build this website.

# Contribution
Feel free to file an issue if you find a bug or want a feature. Pull Request is more than welcome.

# License
This project under MIT license, read [LICENSE](./LICENSE) file for more info.
