CREATE TABLE ideas(
    id                   INT                NOT NULL PRIMARY KEY,
    title                VARCHAR(50)        NOT NULL UNIQUE,
    description          VARCHAR(300)       NOT NULL,
    details              VARCHAR(2000)      NOT NULL,
    registration_date    DATETIME           NOT NULL DEFAULT (DATE('now')),
    owner                INT                NOT NULL REFERENCES users(id)
);

-- CREATE TABLE logs {
--        id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
--        user_id INT NOT NULL,
--        domain_id INT NOT NULL,
--        details VARCHAR(300) NOT NULL
-- }
