CREATE TABLE services(
    id              INT             NOT NULL PRIMARY KEY,
    title           VARCHAR(45)     NOT NULL UNIQUE,
    page_id         VARCHAR(45)     NOT NULL UNIQUE,
    summary         VARCHAR(150)    NOT NULL UNIQUE,
    page_content    TEXT            NOT NULL UNIQUE,
    started_at      DATETIME        DEFAULT (datetime('now'))
)
