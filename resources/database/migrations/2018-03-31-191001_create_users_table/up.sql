CREATE TABLE users(
    id               INT             NOT NULL PRIMARY KEY,
    username         VARCHAR(25)     NOT NULL UNIQUE CHECK (LENGTH(username) >= 3),
    page_id          VARCHAR(25)     NOT NULL UNIQUE,
    first_name       VARCHAR(15)     NOT NULL CHECK (LENGTH(first_name) >= 3),
    last_name        VARCHAR(15)     NOT NULL CHECK (LENGTH(last_name) >= 3),
    permission       INT(3)          NOT NULL DEFAULT 100,
    status           INT(2)          NOT NULL DEFAULT 1,
    phone            VARCHAR(14)     UNIQUE CHECK (phone LIKE '05________%'),
    email            VARCHAR(254)    NOT NULL UNIQUE,
    password         VARCHAR(64)     NOT NULL CHECK (LENGTH(password) >= 8),
    slat             VARCHAR(64)     NOT NULL,
    registered_at    DATETIME        NOT NULL DEFAULT (datetime('now'))
)
