CREATE TABLE clubs(
    id                   INT             NOT NULL PRIMARY KEY,
    clubname             VARCHAR(35)     NOT NULL UNIQUE,
    page_id              VARCHAR(35)     NOT NULL UNIQUE,
    page_content         TEXT            NOT NULL UNIQUE,
    description          VARCHAR(300)    NOT NULL,
    started_at           DATETIME        NOT NULL DEFAULT (datetime('now')),
    scores               INT             NOT NULL DEFAULT 0,
    manager              INT             NOT NULL REFERENCES users(id)
)
