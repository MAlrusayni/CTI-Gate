CREATE TABLE clubs_members(
    club_id              INT                NOT NULL REFERENCES clubs(id),
    user_id              INT                NOT NULL REFERENCES users(id),
    registered_at        DATETIME           NOT NULL DEFAULT (datetime('now')),
    job                  INT(3)             NOT NULL DEFAULT 100,
    CONSTRAINT cns_pk PRIMARY KEY (club_id, user_id)
)
