CREATE TABLE posts_tags(
    id         INT            NOT NULL PRIMARY KEY,
    tag        VARCHAR(50)    NOT NULL,
    post_id    INT            NOT NULL REFERENCES posts(id)
)
