CREATE TABLE courses(
    id             INT             NOT NULL PRIMARY KEY,
    title          VARCHAR(50)     NOT NULL,
    description    VARCHAR(300)    NOT NULL,
    start_at       DATETIME        NOT NULL,
    url            VARCHAR(200)    NOT NULL UNIQUE,
    teacher        INT             REFERENCES users(id)
)
