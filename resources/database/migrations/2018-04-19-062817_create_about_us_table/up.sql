CREATE TABLE about_us(
    id INT NOT NULL PRIMARY KEY,
    title VARCHAR(50) NOT NULL,
    info VARCHAR(400) NOT NULL
)
