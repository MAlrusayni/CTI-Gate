use errors::*;

pub trait CheckedType<T> {
    fn check(value: &T) -> Result<()>;
    fn value(&self) -> &T;
    fn set_value(&mut self, value: T) -> Result<()>;
}

pub trait CheckedAdd<T, Other=Self>: CheckedType<T> {
    type Output;

    fn checked_add(&self, other: Other) -> Result<Self::Output>;
    fn checked_add_assign(&mut self, other: Other) -> Result<()>;
}

pub macro check_add_impl {
    ($t:ident($inner:ident) + primitive $($other:ident)+ -> $output:ident) => {
        $(
            impl CheckedAdd<$inner, $other> for $t {
                type Output = $output;

                fn checked_add(&self, other: $other) -> Result<Self::Output> {
                    let value = *self.value();
                    let value = match value.checked_add(other as $inner) {
                        Some(value) => value,
                        None => return Err(ErrorKind::OverflowCouldHappen.into()),
                    };
                    Self::check(&value)?;
                    Ok($t(value))
                }

                fn checked_add_assign(&mut self, other: $other) -> Result<()> {
                    let value = *self.value();
                    let value = match value.checked_add(other as $inner) {
                        Some(value) => value,
                        None => return Err(ErrorKind::OverflowCouldHappen.into()),
                    };
                    self.set_value(value)?;
                    Ok(())
                }
            }

            impl<'a> CheckedAdd<$inner, &'a $other> for $t {
                type Output = $output;

                fn checked_add(&self, other: &'a $other) -> Result<Self::Output> {
                    let value = *self.value();
                    let value = match value.checked_add(*other as $inner) {
                        Some(value) => value,
                        None => return Err(ErrorKind::OverflowCouldHappen.into()),
                    };
                    Self::check(&value)?;
                    Ok($t(value))
                }

                fn checked_add_assign(&mut self, other: &'a $other) -> Result<()> {
                    let value = *self.value();
                    let value = match value.checked_add(*other as $inner) {
                        Some(value) => value,
                        None => return Err(ErrorKind::OverflowCouldHappen.into()),
                    };
                    self.set_value(value)?;
                    Ok(())
                }
            }
        )+
    },

    ($t:ident($inner:ident) + primitives -> $output:ident) => {
        check_add_impl!($t($inner) + primitive i8 i16 i32 i64 i128 u8 u16 u32 u128 usize isize -> $output);
    },

    ($t:ident($inner:ident) -> $output:ident) => {
        impl CheckedAdd<$inner> for $t {
            type Output = $output;

            fn checked_add(&self, other: Self) -> Result<Self::Output> {
                let value = *self.value();
                let value = match value.checked_add(*other.value()) {
                    Some(value) => value,
                    None => return Err(ErrorKind::OverflowCouldHappen.into()),
                };
                Self::check(&value)?;
                Ok($t(value))
            }

            fn checked_add_assign(&mut self, other: Self) -> Result<()> {
                let value = *self.value();
                let value = match value.checked_add(*other.value()) {
                    Some(value) => value,
                    None => return Err(ErrorKind::OverflowCouldHappen.into()),
                };
                self.set_value(value)?;
                Ok(())
            }
        }

        impl<'a> CheckedAdd<$inner, &'a Self> for $t {
            type Output = $output;

            fn checked_add(&self, other: &'a Self) -> Result<Self::Output> {
                let value = *self.value();
                let value = match value.checked_add(*other.value()) {
                    Some(value) => value,
                    None => return Err(ErrorKind::OverflowCouldHappen.into()),
                };
                Self::check(&value)?;
                Ok($t(value))
            }

            fn checked_add_assign(&mut self, other: &'a Self) -> Result<()> {
                let value = *self.value();
                let value = match value.checked_add(*other.value()) {
                    Some(value) => value,
                    None => return Err(ErrorKind::OverflowCouldHappen.into()),
                };
                self.set_value(value)?;
                Ok(())
            }
        }
    }
}

pub trait CheckedSub<T, Other=Self>: CheckedType<T> {
    type Output;

    fn checked_sub(&self, other: Other) -> Result<Self::Output>;
    fn checked_sub_assign(&mut self, other: Other) -> Result<()>;
}

pub macro check_sub_impl {
    ($t:ident($inner:ident) - primitive $($other:ident)+ -> $output:ident) => {
        $(
            impl CheckedSub<$inner, $other> for $t {
                type Output = $output;

                fn checked_sub(&self, other: $other) -> Result<Self::Output> {
                    let value = *self.value();
                    let value = match value.checked_sub(other as $inner) {
                        Some(value) => value,
                        None => return Err(ErrorKind::OverflowCouldHappen.into()),
                    };
                    Self::check(&value)?;
                    Ok($t(value))
                }

                fn checked_sub_assign(&mut self, other: $other) -> Result<()> {
                    let value = *self.value();
                    let value = match value.checked_sub(other as $inner) {
                        Some(value) => value,
                        None => return Err(ErrorKind::OverflowCouldHappen.into()),
                    };
                    self.set_value(value)?;
                    Ok(())
                }
            }

            impl<'a> CheckedSub<$inner, &'a $other> for $t {
                type Output = $output;

                fn checked_sub(&self, other: &'a $other) -> Result<Self::Output> {
                    let value = *self.value();
                    let value = match value.checked_sub(*other as $inner) {
                        Some(value) => value,
                        None => return Err(ErrorKind::OverflowCouldHappen.into()),
                    };
                    Self::check(&value)?;
                    Ok($t(value))
                }

                fn checked_sub_assign(&mut self, other: &'a $other) -> Result<()> {
                    let value = *self.value();
                    let value = match value.checked_sub(*other as $inner) {
                        Some(value) => value,
                        None => return Err(ErrorKind::OverflowCouldHappen.into()),
                    };
                    self.set_value(value)?;
                    Ok(())
                }
            }
        )+
    },

    ($t:ident($inner:ident) - primitives -> $output:ident) => {
        check_sub_impl!($t($inner) - primitive i8 i16 i32 i64 i128 u8 u16 u32 u128 usize isize -> $output);
    },

    ($t:ident($inner:ident) -> $output:ident) => {
        impl CheckedSub<$inner> for $t {
            type Output = $output;

            fn checked_sub(&self, other: Self) -> Result<Self::Output> {
                let value = *self.value();
                let value = match value.checked_sub(*other.value()) {
                    Some(value) => value,
                    None => return Err(ErrorKind::OverflowCouldHappen.into()),
                };
                Self::check(&value)?;
                Ok($t(value))
            }

            fn checked_sub_assign(&mut self, other: Self) -> Result<()> {
                let value = *self.value();
                let value = match value.checked_sub(*other.value()) {
                    Some(value) => value,
                    None => return Err(ErrorKind::OverflowCouldHappen.into()),
                };
                self.set_value(value)?;
                Ok(())
            }
        }

        impl<'a> CheckedSub<$inner, &'a Self> for $t {
            type Output = $output;

            fn checked_sub(&self, other: &'a Self) -> Result<Self::Output> {
                let value = *self.value();
                let value = match value.checked_sub(*other.value()) {
                    Some(value) => value,
                    None => return Err(ErrorKind::OverflowCouldHappen.into()),
                };
                Self::check(&value)?;
                Ok($t(value))
            }

            fn checked_sub_assign(&mut self, other: &'a Self) -> Result<()> {
                let value = *self.value();
                let value = match value.checked_sub(*other.value()) {
                    Some(value) => value,
                    None => return Err(ErrorKind::OverflowCouldHappen.into()),
                };
                self.set_value(value)?;
                Ok(())
            }
        }
    }
}

pub trait CheckedMul<T, Other=Self>: CheckedType<T> {
    type Output;

    fn checked_mul(&self, other: Other) -> Result<Self::Output>;
    fn checked_mul_assign(&mut self, other: Other) -> Result<()>;
}

pub macro check_mul_impl {
    ($t:ident($inner:ident) * primitive $($other:ident)+ -> $output:ident) => {
        $(
            impl CheckedMul<$inner, $other> for $t {
                type Output = $output;

                fn checked_mul(&self, other: $other) -> Result<Self::Output> {
                    let value = *self.value();
                    let value = match value.checked_mul(other as $inner) {
                        Some(value) => value,
                        None => return Err(ErrorKind::OverflowCouldHappen.into()),
                    };
                    Self::check(&value)?;
                    Ok($t(value))
                }

                fn checked_mul_assign(&mut self, other: $other) -> Result<()> {
                    let value = *self.value();
                    let value = match value.checked_mul(other as $inner) {
                        Some(value) => value,
                        None => return Err(ErrorKind::OverflowCouldHappen.into()),
                    };
                    self.set_value(value)?;
                    Ok(())
                }
            }

            impl<'a> CheckedMul<$inner, &'a $other> for $t {
                type Output = $output;

                fn checked_mul(&self, other: &'a $other) -> Result<Self::Output> {
                    let value = *self.value();
                    let value = match value.checked_mul(*other as $inner) {
                        Some(value) => value,
                        None => return Err(ErrorKind::OverflowCouldHappen.into()),
                    };
                    Self::check(&value)?;
                    Ok($t(value))
                }

                fn checked_mul_assign(&mut self, other: &'a $other) -> Result<()> {
                    let value = *self.value();
                    let value = match value.checked_mul(*other as $inner) {
                        Some(value) => value,
                        None => return Err(ErrorKind::OverflowCouldHappen.into()),
                    };
                    self.set_value(value)?;
                    Ok(())
                }
            }
        )+
    },

    ($t:ident($inner:ident) * primitives -> $output:ident) => {
        check_mul_impl!($t($inner) * primitive i8 i16 i32 i64 i128 u8 u16 u32 u128 usize isize -> $output);
    },

    ($t:ident($inner:ident) -> $output:ident) => {
        impl CheckedMul<$inner> for $t {
            type Output = $output;

            fn checked_mul(&self, other: Self) -> Result<Self::Output> {
                let value = *self.value();
                let value = match value.checked_mul(*other.value()) {
                    Some(value) => value,
                    None => return Err(ErrorKind::OverflowCouldHappen.into()),
                };
                Self::check(&value)?;
                Ok($t(value))
            }

            fn checked_mul_assign(&mut self, other: Self) -> Result<()> {
                let value = *self.value();
                let value = match value.checked_mul(*other.value()) {
                    Some(value) => value,
                    None => return Err(ErrorKind::OverflowCouldHappen.into()),
                };
                self.set_value(value)?;
                Ok(())
            }
        }

        impl<'a> CheckedMul<$inner, &'a Self> for $t {
            type Output = $output;

            fn checked_mul(&self, other: &'a Self) -> Result<Self::Output> {
                let value = *self.value();
                let value = match value.checked_mul(*other.value()) {
                    Some(value) => value,
                    None => return Err(ErrorKind::OverflowCouldHappen.into()),
                };
                Self::check(&value)?;
                Ok($t(value))
            }

            fn checked_mul_assign(&mut self, other: &'a Self) -> Result<()> {
                let value = *self.value();
                let value = match value.checked_mul(*other.value()) {
                    Some(value) => value,
                    None => return Err(ErrorKind::OverflowCouldHappen.into()),
                };
                self.set_value(value)?;
                Ok(())
            }
        }
    }
}

pub trait CheckedDiv<T, Other=Self>: CheckedType<T> {
    type Output;

    fn checked_div(&self, other: Other) -> Result<Self::Output>;
    fn checked_div_assign(&mut self, other: Other) -> Result<()>;
}

pub macro check_div_impl {
    ($t:ident($inner:ident) / primitive $($other:ident)+ -> $output:ident) => {
        $(
            impl CheckedDiv<$inner, $other> for $t {
                type Output = $output;

                fn checked_div(&self, other: $other) -> Result<Self::Output> {
                    let value = *self.value();
                    let value = match value.checked_div(other as $inner) {
                        Some(value) => value,
                        None => return Err(ErrorKind::OverflowCouldHappen.into()),
                    };
                    Self::check(&value)?;
                    Ok($t(value))
                }

                fn checked_div_assign(&mut self, other: $other) -> Result<()> {
                    let value = *self.value();
                    let value = match value.checked_div(other as $inner) {
                        Some(value) => value,
                        None => return Err(ErrorKind::OverflowCouldHappen.into()),
                    };
                    self.set_value(value)?;
                    Ok(())
                }
            }

            impl<'a> CheckedDiv<$inner, &'a $other> for $t {
                type Output = $output;

                fn checked_div(&self, other: &'a $other) -> Result<Self::Output> {
                    let value = *self.value();
                    let value = match value.checked_div(*other as $inner) {
                        Some(value) => value,
                        None => return Err(ErrorKind::OverflowCouldHappen.into()),
                    };
                    Self::check(&value)?;
                    Ok($t(value))
                }

                fn checked_div_assign(&mut self, other: &'a $other) -> Result<()> {
                    let value = *self.value();
                    let value = match value.checked_div(*other as $inner) {
                        Some(value) => value,
                        None => return Err(ErrorKind::OverflowCouldHappen.into()),
                    };
                    self.set_value(value)?;
                    Ok(())
                }
            }
        )+
    },

    ($t:ident($inner:ident) / primitives -> $output:ident) => {
        check_div_impl!($t($inner) / primitive i8 i16 i32 i64 i128 u8 u16 u32 u128 usize isize -> $output);
    },

    ($t:ident($inner:ident) -> $output:ident) => {
        impl CheckedDiv<$inner> for $t {
            type Output = $output;

            fn checked_div(&self, other: Self) -> Result<Self::Output> {
                let value = *self.value();
                let value = match value.checked_div(*other.value()) {
                    Some(value) => value,
                    None => return Err(ErrorKind::OverflowCouldHappen.into()),
                };
                Self::check(&value)?;
                Ok($t(value))
            }

            fn checked_div_assign(&mut self, other: Self) -> Result<()> {
                let value = *self.value();
                let value = match value.checked_div(*other.value()) {
                    Some(value) => value,
                    None => return Err(ErrorKind::OverflowCouldHappen.into()),
                };
                self.set_value(value)?;
                Ok(())
            }
        }

        impl<'a> CheckedDiv<$inner, &'a Self> for $t {
            type Output = $output;

            fn checked_div(&self, other: &'a Self) -> Result<Self::Output> {
                let value = *self.value();
                let value = match value.checked_div(*other.value()) {
                    Some(value) => value,
                    None => return Err(ErrorKind::OverflowCouldHappen.into()),
                };
                Self::check(&value)?;
                Ok($t(value))
            }

            fn checked_div_assign(&mut self, other: &'a Self) -> Result<()> {
                let value = *self.value();
                let value = match value.checked_div(*other.value()) {
                    Some(value) => value,
                    None => return Err(ErrorKind::OverflowCouldHappen.into()),
                };
                self.set_value(value)?;
                Ok(())
            }
        }
    }
}

pub trait CheckedRem<T, Other=Self>: CheckedType<T> {
    type Output;

    fn checked_rem(&self, other: Other) -> Result<Self::Output>;
    fn checked_rem_assign(&mut self, other: Other) -> Result<()>;
}

pub macro check_rem_impl {
    ($t:ident($inner:ident) % primitive $($other:ident)+ -> $output:ident) => {
        $(
            impl CheckedRem<$inner, $other> for $t {
                type Output = $output;

                fn checked_rem(&self, other: $other) -> Result<Self::Output> {
                    let value = *self.value();
                    let value = match value.checked_rem(other as $inner) {
                        Some(value) => value,
                        None => return Err(ErrorKind::OverflowCouldHappen.into()),
                    };
                    Self::check(&value)?;
                    Ok($t(value))
                }

                fn checked_rem_assign(&mut self, other: $other) -> Result<()> {
                    let value = *self.value();
                    let value = match value.checked_rem(other as $inner) {
                        Some(value) => value,
                        None => return Err(ErrorKind::OverflowCouldHappen.into()),
                    };
                    self.set_value(value)?;
                    Ok(())
                }
            }

            impl<'a> CheckedRem<$inner, &'a $other> for $t {
                type Output = $output;

                fn checked_rem(&self, other: &'a $other) -> Result<Self::Output> {
                    let value = *self.value();
                    let value = match value.checked_rem(*other as $inner) {
                        Some(value) => value,
                        None => return Err(ErrorKind::OverflowCouldHappen.into()),
                    };
                    Self::check(&value)?;
                    Ok($t(value))
                }

                fn checked_rem_assign(&mut self, other: &'a $other) -> Result<()> {
                    let value = *self.value();
                    let value = match value.checked_rem(*other as $inner) {
                        Some(value) => value,
                        None => return Err(ErrorKind::OverflowCouldHappen.into()),
                    };
                    self.set_value(value)?;
                    Ok(())
                }
            }
        )+
    },

    ($t:ident($inner:ident) % primitives -> $output:ident) => {
        check_rem_impl!($t($inner) % primitive i8 i16 i32 i64 i128 u8 u16 u32 u128 usize isize -> $output);
    },

    ($t:ident($inner:ident) -> $output:ident) => {
        impl CheckedRem<$inner> for $t {
            type Output = $output;

            fn checked_rem(&self, other: Self) -> Result<Self::Output> {
                let value = *self.value();
                let value = match value.checked_rem(*other.value()) {
                    Some(value) => value,
                    None => return Err(ErrorKind::OverflowCouldHappen.into()),
                };
                Self::check(&value)?;
                Ok($t(value))
            }

            fn checked_rem_assign(&mut self, other: Self) -> Result<()> {
                let value = *self.value();
                let value = match value.checked_rem(*other.value()) {
                    Some(value) => value,
                    None => return Err(ErrorKind::OverflowCouldHappen.into()),
                };
                self.set_value(value)?;
                Ok(())
            }
        }

        impl<'a> CheckedRem<$inner, &'a Self> for $t {
            type Output = $output;

            fn checked_rem(&self, other: &'a Self) -> Result<Self::Output> {
                let value = *self.value();
                let value = match value.checked_rem(*other.value()) {
                    Some(value) => value,
                    None => return Err(ErrorKind::OverflowCouldHappen.into()),
                };
                Self::check(&value)?;
                Ok($t(value))
            }

            fn checked_rem_assign(&mut self, other: &'a Self) -> Result<()> {
                let value = *self.value();
                let value = match value.checked_rem(*other.value()) {
                    Some(value) => value,
                    None => return Err(ErrorKind::OverflowCouldHappen.into()),
                };
                self.set_value(value)?;
                Ok(())
            }
        }
    }
}

pub macro check_ops_impl($t:ident($inner:ident)) {
    check_add_impl!($t($inner) + primitives -> $t);
    check_add_impl!($t($inner) -> $t);
    check_sub_impl!($t($inner) - primitives -> $t);
    check_sub_impl!($t($inner) -> $t);
    check_mul_impl!($t($inner) * primitives -> $t);
    check_mul_impl!($t($inner) -> $t);
    check_div_impl!($t($inner) / primitives -> $t);
    check_div_impl!($t($inner) -> $t);
    check_rem_impl!($t($inner) % primitives -> $t);
    check_rem_impl!($t($inner) -> $t);
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::convert::TryFrom;

    #[derive(PartialEq, Debug)]
    struct Age(i32);

    impl TryFrom<i32> for Age {
        type Error = Error;

        fn try_from(value: i32) -> Result<Self> {
            Age::check(&value)?;
            Ok(Age(value))
        }
    }

    impl CheckedType<i32> for Age {
        fn check(value: &i32) -> Result<()> {
            if *value > 250 {
                return Err("Over regular age".into());
            }

            if *value < 1 {
                return Err("Age cannot be less than 1".into());
            }

            Ok(())
        }

        fn value(&self) -> &i32 {
            &self.0
        }

        fn set_value(&mut self, value: i32) -> Result<()> {
            Age::check(&value)?;
            self.0 = value;
            Ok(())
        }
    }

    check_add_impl!(Age(i32) + primitives -> Age);
    check_add_impl!(Age(i32) -> Age);
    check_sub_impl!(Age(i32) - primitives -> Age);
    check_sub_impl!(Age(i32) -> Age);
    check_mul_impl!(Age(i32) * primitives -> Age);
    check_mul_impl!(Age(i32) -> Age);
    check_div_impl!(Age(i32) / primitives -> Age);
    check_div_impl!(Age(i32) -> Age);
    check_rem_impl!(Age(i32) % primitives -> Age);
    check_rem_impl!(Age(i32) -> Age);

    #[test]
    fn test_checkedtype() {

        let young_age = Age::try_from(18);
        let baby_age = Age::try_from(3);
        let max_age = Age::try_from(250);
        let min_age = Age::try_from(1);
        let over_age = Age::try_from(i32::max_value());

        assert!(Age::try_from(99999999999999999999999).is_err());
        assert!(over_age.is_err());
        assert_eq!(young_age.unwrap(), Age(18));
        assert_eq!(baby_age.unwrap(), Age(3));
        assert_eq!(max_age.unwrap(), Age(250));
        assert_eq!(min_age.unwrap(), Age(1));
    }

    #[test]
    fn test_checked_add() {
        let young_age = Age::try_from(18).unwrap();
        let baby_age = Age::try_from(3).unwrap();
        let max_age = Age::try_from(250).unwrap();

        assert_eq!(baby_age.checked_add(&5).unwrap(), Age(8));
        assert_eq!(baby_age.checked_add(&young_age).unwrap(), Age(21));
        assert!(max_age.checked_add(1).is_err());
    }

    #[test]
    fn test_checked_sub() {
        let young_age = Age::try_from(18).unwrap();
        let baby_age = Age::try_from(3).unwrap();
        let max_age = Age::try_from(250).unwrap();
        let min_age = Age::try_from(1).unwrap();

        assert_eq!(young_age.checked_sub(&5).unwrap(), Age(13));
        assert_eq!(max_age.checked_sub(&baby_age).unwrap(), Age(247));
        assert!(min_age.checked_sub(1).is_err());
    }

    #[test]
    fn test_checked_mul() {
        let young_age = Age::try_from(18).unwrap();
        let baby_age = Age::try_from(3).unwrap();
        let max_age = Age::try_from(250).unwrap();
        let min_age = Age::try_from(1).unwrap();

        assert_eq!(young_age.checked_mul(&2).unwrap(), Age(36));
        assert_eq!(min_age.checked_mul(1).unwrap(), Age(1));
        assert!(max_age.checked_mul(&baby_age).is_err());
    }

    #[test]
    fn test_checked_div() {
        let young_age = Age::try_from(18).unwrap();
        let baby_age = Age::try_from(3).unwrap();
        let max_age = Age::try_from(250).unwrap();
        let min_age = Age::try_from(1).unwrap();

        assert_eq!(young_age.checked_div(&2).unwrap(), Age(9));
        assert_eq!(max_age.checked_div(&baby_age).unwrap(), Age(83));
        assert!(min_age.checked_div(2).is_err());
    }

    #[test]
    fn test_checked_rem() {
        let young_age = Age::try_from(18).unwrap();
        let baby_age = Age::try_from(3).unwrap();
        let max_age = Age::try_from(250).unwrap();
        let min_age = Age::try_from(1).unwrap();

        assert_eq!(young_age.checked_rem(&5).unwrap(), Age(3));
        assert_eq!(max_age.checked_rem(&baby_age).unwrap(), Age(1));
        assert!(min_age.checked_div(5).is_err());
    }
}
