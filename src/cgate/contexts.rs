// Icon strcut that contains icon name and kind to be used in HTML classes
#[derive(Serialize, Clone, Debug)]
pub struct Icon {
    pub name: String,
    pub kind: String
}

impl Icon {
    pub fn new<T: AsRef<str>>(kind: T, name: T) -> Self {
        Self {
            name: name.as_ref().into(),
            kind: kind.as_ref().into()
        }
    }
}

// Link strcut that contains info about any link and can be used in HTML views
#[derive(Serialize, Clone, Debug)]
pub struct Link {
    pub title: String,
    pub icon: Option<Icon>,
    pub url: String,
    pub active: Option<bool>
}

impl Link {
    pub fn new<T: AsRef<str>>(title: T, icon: Option<Icon>, url: T, active: Option<bool>) -> Self {
        Self {
            title: title.as_ref().to_string(),
            icon: icon,
            url: url.as_ref().to_string(),
            active: active
        }
    }

    pub fn active<T: AsRef<str>>(title: T, icon: Option<Icon>, url: T) -> Self {
        Link::new(title, icon, url, Some(true))
    }

    pub fn normal<T: AsRef<str>>(title: T, icon: Option<Icon>, url: T) -> Self {
        Link::new(title, icon, url, None)
    }

    pub fn to_active(&mut self) {
        self.active = Some(true);
    }

    pub fn to_normal(&mut self) {
        self.active = None;
    }
}
