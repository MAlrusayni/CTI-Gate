use errors::*;
use diesel::sql_types::*;
use macros::*;
use checked_types::*;
use std::convert::TryFrom;
use rocket::{
    request::FromFormValue,
    http::RawStr
};

pub use database::types::{Id, Title};

#[derive(Debug, Clone, Serialize, Deserialize, AsExpression, FromSqlRow)]
#[sql_type = "Text"]
pub struct Info(String);

try_from_strings!(Info);
from_impl!(Info(String) -> Strings);
from_sql_impl!(Text -> Info(String));
to_sql_impl!(Info(String) -> Text);
display_impl!(Info(String));

impl CheckedType<String> for Info {
    fn check(value: &String) -> Result<()> {
        let chars_len = value.chars().count();

        // check if summary is only whitespaces, return error if it is.
        if value.is_whitespace() || value.is_empty() {
            return Err(ErrorKind::EmptyInfo)?;
        }

        // check the length, return error if it is short.
        if chars_len < 15 {
            return Err(ErrorKind::ShortInfo(value.to_owned()))?;
        }

        // check the length > 400, retnr error if it is long.
        if chars_len > 400 {
            return Err(ErrorKind::LongInfo(value.to_owned()))?;
        }

        // return Ok(()) if all validation checks did pass
        Ok(())
    }

    fn value(&self) -> &String {
        &self.0
    }

    fn set_value(&mut self, value: String) -> Result<()> {
        Self::check(&value)?;
        self.0 = value;
        Ok(())
    }
}

impl<'v> FromFormValue<'v> for Info {
    type Error = Error;

    fn from_form_value(value: &'v RawStr) -> Result<Self> {
        let msg = format!("Couldn't parse info '{}' from HTML form", value);
        let value = value.url_decode()
            .chain_err(|| msg)?;
        Self::try_from(value)
    }
}
