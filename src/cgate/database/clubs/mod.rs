use self::types::*;
use errors::*;
use diesel::prelude::*;
use pages::Pagination;
use database::DatabaseConnection;
use std::ops::Deref;
use super::users::{User, UserContext};

pub mod types;

#[derive(Queryable, Debug, Clone, Serialize, Deserialize)]
pub struct Club {
    id: Id,
    clubname: ClubName,
    page_id: PageId,
    page_content: PageContent,
    description: Description,
    started_at: DateTime,
    scores: Scores,
    manager: i32,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ClubContext {
    clubname: ClubName,
    page_id: PageId,
    page_content: PageContent,
    description: Description,
    started_at: DateTime,
    scores: Scores,
    manager: Option<UserContext>,
}

impl From<Club> for ClubContext {
    fn from(club: Club) -> Self {
        Self {
            clubname: club.clubname,
            page_id: club.page_id,
            page_content: club.page_content,
            description: club.description,
            started_at: club.started_at,
            scores: club.scores,
            manager: None,
        }
    }
}

impl From<(Club, User)> for ClubContext {
    fn from(club_data: (Club, User)) -> Self {
        let mut club_context: Self = club_data.0.into();
        club_context.manager = Some(club_data.1.into());
        club_context
    }
}

#[derive(Queryable, Debug)]
pub struct ClubMember {
    club_id: i32,
    user_id: i32,
    registered_at: DateTime,
    job: i32,
}


pub trait ClubsDb: DatabaseConnection {
    fn get_clubs_number(&self) -> Result<u32> {
        use database::schema::clubs::dsl::*;

        let clubs_number = clubs.count()
            .load::<i64>(self.db_connection().deref())
            .chain_err(|| "Couldn't get clubs number from db")?
            .pop();

        match clubs_number {
            None => Err(ErrorKind::NoClubsAvailable.into()),
            Some(number) => Ok(number as u32),
        }
    }

    fn get_last_clubs_context(&self, number: i64) -> Result<Vec<ClubContext>> {
        use database::schema::{clubs, users};

        let join = clubs::table.inner_join(users::table);

        let clubs = join.limit(number)
            .load::<(Club, User)>(self.db_connection().deref())
            .chain_err(|| "Couldn't load clubs from db")?
            .iter()
            .map(|club_data| club_data.clone().into())
            .collect();

        Ok(clubs)
    }

    fn get_clubs_context(&self) -> Result<Vec<ClubContext>>
    where
        Self: Pagination,
    {
        use database::schema::{users, clubs};
        let page = self.page();
        let items_per_page = self.items_per_page();
        let conn = self.db_connection();

        self.is_vaild_page_number(page)?;

        let join = clubs::table.inner_join(users::table);

        let offset = items_per_page * (page as i32 - 1).abs() as u32;

        let clubs = join.limit(items_per_page as i64)
            .offset(offset as i64)
            .load::<(Club, User)>(conn.deref())
            .chain_err(|| "Couldn't get clubs info from db")?
            .into_iter()
            .map(|club_data| club_data.into())
            .collect();

        Ok(clubs)
    }
}
