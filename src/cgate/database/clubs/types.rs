use errors::*;
use macros::*;
use checked_types::*;
use diesel::sql_types::*;
use std::convert::TryFrom;
use rocket::{
    request::FromFormValue,
    http::RawStr
};

pub use database::types::{Id, DateTime, PageId, PageContent, Description};

#[derive(Debug, Clone, Serialize, Deserialize, AsExpression, FromSqlRow)]
#[sql_type = "Text"]
pub struct ClubName(String);

try_from_strings!(ClubName);
from_impl!(ClubName(String) -> Strings);
from_sql_impl!(Text -> ClubName(String));
to_sql_impl!(ClubName(String) -> Text);
display_impl!(ClubName(String));

impl CheckedType<String> for ClubName {
    fn check(value: &String) -> Result<()> {
        let chars_len = value.chars().count();

        // check if the club very long, return error if it is.
        if chars_len > 35 {
            return Err(ErrorKind::LongClubName(value.to_owned()))?;
        }

        if chars_len < 3 {
            return Err(ErrorKind::ShortClubName(value.to_owned()))?;
        }

        // check if username is only whitespaces, return error if it is.
        if value.is_whitespace() || value.is_empty() {
            return Err(ErrorKind::EmptyClubName)?;
        }

        // check if username start or end with whitespace, return error if it is.
        if value.starts_with(char::is_whitespace) || value.ends_with(char::is_whitespace) {
            return Err(ErrorKind::ClubNameStartOrEndWithWihitespace(value.to_owned()))?;
        }

        // check if username contains any non alphanumeric character, return error it it does.
        let words = value.split_whitespace();
        for word in words {
            if !word.is_alphanumeric() {
                return Err(ErrorKind::ClubNameContainNonAlphanumeric(value.to_owned()))?;
            }
        }

        // return Ok(()) if all validation checks did pass
        Ok(())
    }

    fn value(&self) -> &String {
        &self.0
    }

    fn set_value(&mut self, value: String) -> Result<()> {
        Self::check(&value)?;
        self.0 = value;
        Ok(())
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, AsExpression, FromSqlRow)]
#[sql_type = "Integer"]
pub struct Scores(i32);

try_from_impl!(i8 i16 i32 u8 u16 -> Scores(i32));
from_impl!(Scores(i32) -> i32 i64 i128 u32 u64 u128 usize isize);
display_impl!(Scores(i32));
from_sql_impl!(Integer -> Scores(i32));
to_sql_impl!(Scores(i32) -> Integer);
check_ops_impl!(Scores(i32));

impl CheckedType<i32> for Scores {
    fn check(_value: &i32) -> Result<()> {
        Ok(())
    }

    fn value(&self) -> &i32 {
        &self.0
    }

    fn set_value(&mut self, value: i32) -> Result<()> {
        Self::check(&value)?;
        self.0 = value;
        Ok(())
    }
}

impl<'v> FromFormValue<'v> for Scores {
    type Error = Error;

    fn from_form_value(value: &'v RawStr) -> Result<Self> {
        let msg = format!("Couldn't parse scores '{}' from HTML form", value);
        let value = value.parse::<i32>()
            .chain_err(|| msg)?;
        Self::try_from(value)
    }
}
