use diesel::sql_types::*;
use errors::*;
use std::convert::TryFrom;
use rocket::request::FromFormValue;
use rocket::http::RawStr;
use macros::*;
use checked_types::*;

pub use database::users::types::{Username, Email, Phone};
pub use database::types::{Id, DateTime};

#[derive(Debug, Clone, Copy, AsExpression, FromSqlRow, Serialize, Deserialize)]
#[sql_type = "Integer"]
pub enum Category {
    Others,
    News,
    Clubs,
    Courses,
    Patents,
}

to_sql_impl_for_enum!(Category as i32 -> Integer);
from_sql_impl!(Integer -> Category(i32));

impl TryFrom<i32> for Category {
    type Error = Error;

    fn try_from(value: i32) -> Result<Self> {
        let status = match value {
            0 => Category::Others,
            1 => Category::Patents,
            2 => Category::Clubs,
            3 => Category::Courses,
            4 => Category::News,
            _ => return Err(ErrorKind::UnknownContactUsMessageCategory(value))?,
        };
        Ok(status)
    }
}

impl From<Category> for i32 {
    fn from(value: Category) -> i32 {
        match value {
            Category::Others => 0,
            Category::Patents => 1,
            Category::Clubs => 2,
            Category::Courses => 3,
            Category::News => 4,
        }
    }
}

impl Default for Category {
    fn default() -> Self { Category::Others }
}

impl<'v> FromFormValue<'v> for Category {
    type Error = Error;

    fn from_form_value(form_value: &'v RawStr) -> Result<Category> {
        let value = form_value.parse::<i32>()
            .chain_err(|| "Please pick a proper category")?;
        Category::try_from(value)
    }
}

#[derive(Debug, Clone, Copy, AsExpression, FromSqlRow, Serialize, Deserialize)]
#[sql_type = "Integer"]
pub enum Status {
    Waiting,
    Replayed,
    Archived,
}

to_sql_impl_for_enum!(Status as i32 -> Integer);
from_sql_impl!(Integer -> Status(i32));

impl TryFrom<i32> for Status {
    type Error = Error;

    fn try_from(value: i32) -> Result<Self> {
        let status = match value {
            0 => Status::Waiting,
            1 => Status::Replayed,
            2 => Status::Archived,
            _ => return Err(ErrorKind::UnknownContactUsMessageStatus(value))?,
        };
        Ok(status)
    }
}

impl From<Status> for i32 {
    fn from(value: Status) -> i32 {
        match value {
            Status::Waiting => 0,
            Status::Replayed => 1,
            Status::Archived => 2,
        }
    }
}

impl Default for Status {
    fn default() -> Self { Status::Waiting }
}

#[derive(Debug, Clone, Serialize, Deserialize, AsExpression, FromSqlRow)]
#[sql_type = "Text"]
pub struct Subject(String);

try_from_strings!(Subject);
from_impl!(Subject(String) -> Strings);
from_sql_impl!(Text -> Subject(String));
to_sql_impl!(Subject(String) -> Text);
display_impl!(Subject(String));

impl CheckedType<String> for Subject {
    fn check(value: &String) -> Result<()> {
        // check if value is whitespaces or empty, return error if it is.
        if value.is_whitespace() || value.is_empty() {
            return Err(ErrorKind::EmptySubject)?;
        }

        // check if subject is very long or not
        if value.chars().count() > 50 {
            return Err(ErrorKind::LongSubject(value.to_owned()))?;
        }

        // check if username start or end with whitespace, return error if it is.
        if value.starts_with(char::is_whitespace) || value.ends_with(char::is_whitespace) {
            return Err(ErrorKind::SubjectStartsOrEndWithWhitespace(value.to_owned()))?;
        }

        // return Ok(()) if all validation checks did pass
        Ok(())
    }

    fn value(&self) -> &String {
        &self.0
    }

    fn set_value(&mut self, value: String) -> Result<()> {
        Self::check(&value)?;
        self.0 = value;
        Ok(())
    }
}

impl<'v> FromFormValue<'v> for Subject {
    type Error = Error;

    fn from_form_value(value: &'v RawStr) -> Result<Subject> {
        let msg = format!("Couldn't parse subject '{}' from HTML form", value);
        let value = value.url_decode()
            .chain_err(|| msg)?;
        Self::try_from(value)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, AsExpression, FromSqlRow)]
#[sql_type = "Text"]
pub struct Content(String);

try_from_strings!(Content);
from_impl!(Content(String) -> Strings);
from_sql_impl!(Text -> Content(String));
to_sql_impl!(Content(String) -> Text);
display_impl!(Content(String));

impl CheckedType<String> for Content {
    fn check(value: &String) -> Result<()> {
        // check if value is whitespaces or empty, return error if it is.
        if value.is_whitespace() || value.is_empty() {
            return Err(ErrorKind::EmptyContent)?;
        }

        // check if subject is very long or not
        if value.chars().count() > 300 {
            return Err(ErrorKind::LongContent(value.to_owned()))?;
        }

        // check if value start or end with whitespace, return error if it is.
        if value.starts_with(char::is_whitespace) || value.ends_with(char::is_whitespace) {
            return Err(ErrorKind::ContentStartsOrEndWithWhitespace(value.to_owned()))?;
        }

        // return Ok(()) if all validation checks did pass
        Ok(())
    }

    fn value(&self) -> &String {
        &self.0
    }

    fn set_value(&mut self, value: String) -> Result<()> {
        Self::check(&value)?;
        self.0 = value;
        Ok(())
    }
}

impl<'v> FromFormValue<'v> for Content {
    type Error = Error;

    fn from_form_value(value: &'v RawStr) -> Result<Content> {
        let msg = format!("Couldn't parse content '{}' from HTML form", value);
        let value = value.url_decode()
            .chain_err(|| msg)?;
        Self::try_from(value)
    }
}
