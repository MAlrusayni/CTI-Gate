use self::types::*;
use errors::*;
use diesel::prelude::*;
use database::DatabaseConnection;
use pages::Pagination;
use std::ops::Deref;
use super::users::{User, UserContext};

pub mod types;

#[derive(Queryable, Debug, Clone, Serialize, Deserialize)]
pub struct Course {
    id: Id,
    title: Title,
    description: Description,
    start_at: DateTime,
    url: Url,
    teacher: Option<i32>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CourseContext {
    title: Title,
    description: Description,
    start_at: DateTime,
    url: Url,
    teacher: Option<UserContext>,
}

impl From<Course> for CourseContext {
    fn from(course: Course) -> Self {
        Self {
            title: course.title,
            description: course.description,
            start_at: course.start_at,
            url: course.url,
            teacher: None,
        }
    }
}

impl From<(Course, Option<User>)> for CourseContext {
    fn from(course_data: (Course, Option<User>)) -> Self {
        let mut course_context: Self = course_data.0.into();
        course_context.teacher = course_data.1.map(|teacher| teacher.into());
        course_context
    }
}

#[derive(Queryable, Debug, Serialize)]
pub struct CourseTag {
    id: Id,
    tag: String,
    course_id: i32,
}


pub trait CoursesDb: DatabaseConnection {
    /// return how many courses in the database
    fn get_courses_number(&self) -> Result<u32> {
        use database::schema::courses::dsl::*;

        let courses_number = courses.count()
            .load::<i64>(self.db_connection().deref())
            .chain_err(|| "Couldn't get courses number from db")?
            .pop();

        match courses_number {
            None => Err(ErrorKind::NoCoursesAvailable.into()),
            Some(number) => Ok(number as u32),
        }
    }

    ///
    fn get_courses_context(&self) -> Result<Vec<CourseContext>>
    where
        Self: Pagination,
    {
        use database::schema::{users, courses};
        let page = self.page();
        let items_per_page = self.items_per_page();
        let conn = self.db_connection();

        self.is_vaild_page_number(page)?;

        let join = courses::table.left_join(users::table);

        let offset = items_per_page * (page as i32 - 1).abs() as u32;

        let courses = join.limit(items_per_page as i64)
            .offset(offset as i64)
            .load::<(Course, Option<User>)>(conn.deref())
            .chain_err(|| "Couldn't get courses info from db")?
            .into_iter()
            .map(|course_data| course_data.into())
            .collect();

        Ok(courses)
    }


    ///
    fn get_last_courses_context(&self, number: i64) -> Result<Vec<CourseContext>> {
        use database::schema::{courses, users};

        let join = courses::table.left_join(users::table);

        let courses = join.limit(number)
            .order(courses::start_at.asc())
            .load::<(Course, Option<User>)>(self.db_connection().deref())
            .chain_err(|| "Couldn't load courses from db")?
            .iter()
            .map(|course_data| course_data.clone().into())
            .collect();

        Ok(courses)
    }
}
