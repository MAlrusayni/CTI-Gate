use self::types::*;
use errors::*;
use checked_types::*;
use diesel::prelude::*;
use std::convert::TryFrom;
use pages::Pagination;
use std::ops::Deref;
use pages::dashboard::news_add::NewPostForm;
use pages::dashboard::news_edit::EditPostForm;
use database::{
    DatabaseConnection,
    users::{User, UserContext},
    clubs::{Club, ClubContext},
    schema::posts as posts_table
};

pub mod types;

#[derive(Queryable, Insertable, Debug, Clone, Serialize, Deserialize)]
#[table_name = "posts_table"]
pub struct Post {
    id: Id,
    title: Title,
    summary: Summary,
    page_id: PageId,
    page_content: PageContent,
    published_at: DateTime,
    draft: bool,
    author: i32,
    club: Option<i32>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PostContext {
    id: Id,
    title: Title,
    summary: Summary,
    page_id: PageId,
    page_content: PageContent,
    published_at: DateTime,
    draft: bool,
    author: Option<UserContext>,
    club: Option<ClubContext>,
}

impl From<Post> for PostContext {
    fn from(post: Post) -> Self {
        Self {
            id: post.id,
            title: post.title,
            summary: post.summary,
            page_id: post.page_id,
            page_content: post.page_content,
            published_at: post.published_at,
            draft: post.draft,
            author: None,
            club: None,
        }
    }
}

impl From<(Post, User, Option<Club>)> for PostContext {
    fn from(post_data: (Post, User, Option<Club>)) -> Self {
        let mut post_context: Self = post_data.0.into();
        post_context.author = Some(post_data.1.into());
        post_context.club = post_data.2.map(|club| club.into());
        post_context
    }
}

#[derive(Queryable, Debug, Serialize)]
pub struct PostTag {
    id: Id,
    tag: String,
    post_id: i32,
}

pub trait PostsDb: DatabaseConnection {
    /// return how posts number that are not draft
    fn get_posts_number(&self) -> Result<u32> {
        use database::schema::posts::dsl::*;

        let posts_number = posts.filter(draft.eq(false))
            .count()
            .load::<i64>(self.db_connection().deref())
            .chain_err(|| "Couldn't get posts numbers from db")?
            .pop();

        // FIXME: this should return 0 insted of Error NoPostsAvailable
        match posts_number {
            None => Err(ErrorKind::NoPostsAvailable.into()),
            Some(number) => Ok(number as u32),
        }
    }

    ///
    fn get_draft_posts_number(&self) -> Result<u32> {
        use database::schema::posts::dsl::*;

        let draft_posts_number = posts.filter(draft.eq(true))
            .count()
            .load::<i64>(self.db_connection().deref())
            .chain_err(|| "Couldn't get draft posts numbers from db")?
            .pop();

        match draft_posts_number {
            None => Ok(0),
            Some(number) => Ok(number as u32),
        }
    }

    ///
    fn get_posts_context(&self) -> Result<Vec<PostContext>>
    where
        Self: Pagination,
    {
        use database::schema::{posts, users, clubs};
        use diesel::dsl::max;

        let page = self.page();
        let items_per_page = self.items_per_page();
        let conn = self.db_connection();

        self.is_vaild_page_number(page)?;

        let join = posts::table.inner_join(users::table)
            .left_join(clubs::table);

        let offset = items_per_page * (page as i32 - 1).abs() as u32;

        let posts = join.order(posts::published_at.desc())
            .filter(posts::draft.eq(false))
            .limit(items_per_page as i64)
            .offset(offset as i64)
            .load::<(Post, User, Option<Club>)>(conn.deref())
            .chain_err(|| format!("Couldn't load posts for news/page/{} from db", page))?
            .into_iter()
            .map(|post_data| post_data.clone().into())
            .collect();

        Ok(posts)
    }

    ///
    fn get_last_posts_context(&self, number: i64) -> Result<Vec<PostContext>> {
        use database::schema::{posts, users, clubs};

        let join = posts::table.inner_join(users::table)
            .left_join(clubs::table);

        let posts = join.filter(posts::draft.eq(false))
            .limit(number)
            .load::<(Post, User, Option<Club>)>(self.db_connection().deref())
            .chain_err(|| "Couldn't load posts from db")?
            .iter()
            .map(|post_data| post_data.clone().into())
            .collect();

        Ok(posts)
    }

    ///
    fn get_last_posts_context_by_auther(
        &self,
        id: &Id,
        number: i64,
    ) -> Result<Vec<PostContext>> {
        use database::schema::{posts, users, clubs};

        let join = posts::table.inner_join(users::table)
            .left_join(clubs::table);

        let posts = join.filter(posts::draft.eq(false))
            .filter(posts::author.eq(id))
            .limit(number)
            .load::<(Post, User, Option<Club>)>(self.db_connection().deref())
            .chain_err(|| "Couldn't load posts from db")?
            .into_iter()
            .map(|post| post.into())
            .collect();

        Ok(posts)
    }

    ///
    fn get_posts_number_written_by_auther(&self, id: &Id) -> Result<u32> {
        use database::schema::posts;

        let posts_number = posts::table.filter(posts::author.eq(id))
            .count()
            .load::<i64>(self.db_connection().deref())
            .chain_err(|| "Couldn't get posts number written by auther from db")?
            .pop();

        match posts_number {
            None => Ok(0),
            Some(number) => Ok(number as u32),
        }
    }

    /// get `PostContext` by `PageId`
    fn get_post_context_by_page_id(&self, page_id: &PageId) -> Result<PostContext> {
        use database::schema::{posts, users, clubs};

        let join = posts::table.inner_join(users::table)
            .left_join(clubs::table);

        let post = join.filter(posts::page_id.eq(page_id))
            .first::<(Post, User, Option<Club>)>(self.db_connection().deref())
            .chain_err(|| "Couldn't get post by page id from db")?
            .into();

        Ok(post)
    }

    fn get_max_id(&self) -> Result<Option<Id>> {
        use database::schema::posts::dsl::*;
        use diesel::dsl::max;

        let max_id = posts.order(id)
            .select(max(id))
            .first::<Option<Id>>(self.db_connection().deref())?;

        Ok(max_id)
    }

    fn check_title_is_available(&self, value: &Title) -> Result<()> {
        use database::schema::posts::dsl::*;

        let is_used = posts.filter(title.eq(value))
            .count()
            .load::<i64>(self.db_connection().deref())
            .chain_err(|| "Couldn't check if title is used or not from db")?
            .pop();

        match is_used {
            None | Some(0) => Ok(()),
            Some(_) => Err(ErrorKind::NotAvailableTitle(value.to_string()))?,
        }
    }

    fn check_summary_is_available(&self, value: &Summary) -> Result<()> {
        use database::schema::posts::dsl::*;

        let is_used = posts.filter(summary.eq(value))
            .count()
            .load::<i64>(self.db_connection().deref())
            .chain_err(|| "Couldn't check if summary is used or not from db")?
            .pop();

        match is_used {
            None | Some(0) => Ok(()),
            Some(_) => Err(ErrorKind::NotAvailableSummary(value.to_string()))?,
        }
    }

    fn check_page_id_is_available(&self, value: &PageId) -> Result<()> {
        use database::schema::posts::dsl::*;

        let is_used = posts.filter(page_id.eq(value))
            .count()
            .load::<i64>(self.db_connection().deref())
            .chain_err(|| "Couldn't check if summary is used or not from db")?
            .pop();

        match is_used {
            None | Some(0) => Ok(()),
            Some(_) => Err(ErrorKind::NotAvailablePageId(value.to_string()))?,
        }
    }

    ///
    fn delete_post(&self, pid: &PageId) -> Result<()> {
        use diesel::delete;
        use database::schema::posts::dsl::*;

        delete(posts.filter(page_id.eq(pid)))
            .execute(self.db_connection().deref())
            .chain_err(|| "Couldn't delete post from db")?;

        Ok(())
    }

    ///
    fn update_post(&self, pid: &PageId, form: &EditPostForm) -> Result<()> {
        use diesel::update;
        use database::schema::posts::dsl::*;

        let new_title = EditPostForm::ok_or_incomplete_form(&form.title)?;
        let new_summary = EditPostForm::ok_or_incomplete_form(&form.summary)?;
        let new_page_content = EditPostForm::ok_or_incomplete_form(&form.page_content)?;
        let new_draft = form.draft;

        let _ = update(posts.filter(page_id.eq(pid)))
            .set((
                title.eq(new_title),
                summary.eq(new_summary),
                page_content.eq(new_page_content),
                draft.eq(new_draft)
            ))
            .execute(self.db_connection().deref())
            .chain_err(|| "Couldn't update post from db")?;

        Ok(())
    }

    // TODO: add club author
    /// add new post to the database from NewPostForm
    fn add_new_post(&self, new_post: &NewPostForm, author: &Id) -> Result<()> {
        use diesel::{self, select};

        let title = NewPostForm::ok_or_incomplete_form(&new_post.title)?;
        let summary = NewPostForm::ok_or_incomplete_form(&new_post.summary)?;
        let page_content = NewPostForm::ok_or_incomplete_form(&new_post.page_content)?;
        let draft = new_post.draft;

        self.check_title_is_available(&title)?;
        self.check_summary_is_available(&summary)?;

        let page_id = PageId::new_from_title(&title)?;
        self.check_page_id_is_available(&page_id)?;

        let id = match self.get_max_id()? {
            Some(value) => value.checked_add(1)?,
            None => Id::try_from(0)?,
        };

        let published_at = select(diesel::dsl::now)
            .first::<DateTime>(self.db_connection().deref())?;

        let club = None;
        let author = *author.value();

        let post = Post {
            id,
            title,
            summary,
            page_id,
            page_content,
            published_at,
            draft,
            author,
            club,
        };

        // insert the post into posts table in the database
        {
            use diesel::insert_into;
            use database::schema::posts::dsl::*;
            insert_into(posts)
                .values(post)
                .execute(self.db_connection().deref())
                .chain_err(|| "Couldn't add a new post to the db")?;
        }

        Ok(())
    }
}
