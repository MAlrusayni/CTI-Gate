use self::types::*;
use errors::*;
use diesel::prelude::*;
use database::DatabaseConnection;
use std::ops::Deref;

pub mod types;

#[derive(Queryable, Debug, Clone, Serialize, Deserialize)]
pub struct Service {
    pub id: Id,
    pub title: Title,
    pub page_id: PageId,
    pub summary: Summary,
    pub page_content: PageContent,
    pub started_at: Option<DateTime>
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ServiceContext {
    pub id: Id,
    pub title: Title,
    pub page_id: PageId,
    pub summary: Summary,
    pub page_content: PageContent,
    pub started_at: Option<DateTime>
}

impl From<Service> for ServiceContext {
    fn from(service: Service) -> Self {
        Self {
            id: service.id,
            title: service.title,
            page_id: service.page_id,
            summary: service.summary,
            page_content: service.page_content,
            started_at: service.started_at,
        }
    }
}

pub trait ServicesDb: DatabaseConnection {
    fn get_services_context(&self) -> Result<Vec<ServiceContext>> {
        use database::schema::services::dsl::*;

        let service = services.load::<Service>(self.db_connection().deref())
            .chain_err(|| "Coudn't load services from db")?
            .into_iter()
            .map(|serv| ServiceContext::from(serv))
            .collect();

        Ok(service)
    }
}
