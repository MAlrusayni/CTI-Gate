use macros::*;
use checked_types::*;
use diesel::sql_types::*;
use errors::*;
use std::convert::TryFrom;
use database::users;
use chrono::NaiveDateTime;
use rocket::{
    request::{FromFormValue, FromParam},
    http::RawStr,
};

#[derive(Debug, Clone, Serialize, Deserialize, AsExpression, FromSqlRow)]
#[sql_type = "Integer"]
pub struct Id(i32);

deref_impl!(Id(i32));
try_from_impl!(i8 i16 i32 u8 u16 -> Id(i32));
from_impl!(Id(i32) -> i32 i64 i128 u32 u64 u128 usize isize);
display_impl!(Id(i32));
from_sql_impl!(Integer -> Id(i32));
to_sql_impl!(Id(i32) -> Integer);
check_ops_impl!(Id(i32));

impl CheckedType<i32> for Id {
    fn check(value: &i32) -> Result<()> {
        if *value < 0 {
            return Err(ErrorKind::NegativeId(*value))?;
        }

        Ok(())
    }

    fn value(&self) -> &i32 {
        &self.0
    }

    fn set_value(&mut self, value: i32) -> Result<()> {
        Self::check(&value)?;
        self.0 = value;
        Ok(())
    }
}

impl<'v> FromFormValue<'v> for Id {
    type Error = Error;

    fn from_form_value(value: &'v RawStr) -> Result<Self> {
        let msg = format!("Couldn't parse Id '{}' from HTML form", value);
        let value = value.parse::<i32>()
            .chain_err(|| msg)?;
        Self::try_from(value)
    }
}

impl<'a> FromParam<'a> for Id {
    type Error = Error;

    fn from_param(param: &'a RawStr) -> Result<Id> {
        Id::try_from(param.parse::<i32>()?)
    }
}


#[derive(Debug, Clone, Serialize, Deserialize, AsExpression, FromSqlRow)]
#[sql_type = "Timestamp"]
pub struct DateTime(NaiveDateTime);

from_sql_impl!(Timestamp -> DateTime(NaiveDateTime));
to_sql_impl!(DateTime(NaiveDateTime) -> Timestamp);
try_from_impl!(NaiveDateTime -> DateTime(NaiveDateTime));
from_impl!(DateTime(NaiveDateTime) -> NaiveDateTime);
display_impl!(DateTime(NaiveDateTime));


impl CheckedType<NaiveDateTime> for DateTime {
    fn check(_value: &NaiveDateTime) -> Result<()> {
        Ok(())
    }

    fn value(&self) -> &NaiveDateTime {
        &self.0
    }

    fn set_value(&mut self, value: NaiveDateTime) -> Result<()> {
        DateTime::check(&value)?;
        self.0 = value;
        Ok(())
    }
}

impl Default for DateTime {
    fn default() -> Self {
        use chrono::Utc;
        DateTime::try_from(Utc::now().naive_utc()).unwrap()
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, AsExpression, FromSqlRow)]
#[sql_type = "Text"]
pub struct PageId(String);

deref_impl!(PageId(String));
try_from_strings!(PageId);
from_impl!(PageId(String) -> Strings);
from_sql_impl!(Text -> PageId(String));
to_sql_impl!(PageId(String) -> Text);
display_impl!(PageId(String));

impl CheckedType<String> for PageId {
    fn check(value: &String) -> Result<()> {
        // check if page_id is only whitespaces, return error if it is.
        if value.is_whitespace() || value.is_empty() {
            return Err(ErrorKind::EmptyPageId)?;
        }

        // check if page_id contains whitespace, return error if it is.
        if value.contains(char::is_whitespace) {
            return Err(ErrorKind::PageIdContainsWhitespace(value.to_owned()))?;
        }

        if value.contains(|c: char| !c.is_alphanumeric() && c != '-' && c != '_') {
            return Err(ErrorKind::PageIdContainsNotAllowedCharacter(value.to_owned()))?;
        }

        // return Ok(()) if all validation checks did pass
        Ok(())
    }

    fn value(&self) -> &String {
        &self.0
    }

    fn set_value(&mut self, value: String) -> Result<()> {
        Self::check(&value)?;
        self.0 = value;
        Ok(())
    }
}

impl PageId {
    pub fn new_from_username(username: &users::types::Username) -> Result<Self> {
        let page_id = username.replace(char::is_whitespace, "-");
        Self::try_from(page_id)
    }

    pub fn new_from_title(title: &Title) -> Result<Self> {
        let page_id = title.replace(char::is_whitespace, "-");
        Self::try_from(page_id)
    }
}

impl<'a> FromParam<'a> for PageId {
    type Error = Error;

    fn from_param(param: &'a RawStr) -> Result<PageId> {
        PageId::try_from(param.url_decode()?)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, AsExpression, FromSqlRow)]
#[sql_type = "Text"]
pub struct PageContent(String);

try_from_strings!(PageContent);
from_impl!(PageContent(String) -> Strings);
from_sql_impl!(Text -> PageContent(String));
to_sql_impl!(PageContent(String) -> Text);
display_impl!(PageContent(String));

impl CheckedType<String> for PageContent {
    fn check(value: &String) -> Result<()> {
        // check the length, return error if it is short.
        if value.chars().count() < 3 {
            return Err(ErrorKind::ShortPageContent(value.to_owned()))?;
        }

        // check if summary is only whitespaces, return error if it is.
        if value.is_whitespace() || value.is_empty() {
            return Err(ErrorKind::EmptyPageContent)?;
        }

        // return Ok(()) if all validation checks did pass
        Ok(())
    }

    fn value(&self) -> &String {
        &self.0
    }

    fn set_value(&mut self, value: String) -> Result<()> {
        Self::check(&value)?;
        self.0 = value;
        Ok(())
    }
}

impl<'v> FromFormValue<'v> for PageContent {
    type Error = Error;

    fn from_form_value(value: &'v RawStr) -> Result<Self> {
        let msg = format!("Couldn't parse page content '{}' from HTML form", value);
        let value = value.url_decode()
            .chain_err(|| msg)?;
        Self::try_from(value)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, AsExpression, FromSqlRow)]
#[sql_type = "Text"]
pub struct Description(String);

try_from_strings!(Description);
from_impl!(Description(String) -> Strings);
from_sql_impl!(Text -> Description(String));
to_sql_impl!(Description(String) -> Text);
display_impl!(Description(String));

impl CheckedType<String> for Description {
    fn check(value: &String) -> Result<()> {
        let chars_len = value.chars().count();

        // check if page_id is only whitespaces, return error if it is.
        if value.is_whitespace() || value.is_empty() {
            return Err(ErrorKind::EmptyDescription)?;
        }

        if chars_len > 300 {
            return Err(ErrorKind::LongDescription)?;
        }

        if chars_len < 10 {
            return Err(ErrorKind::ShortDescription(value.to_owned()))?;
        }

        // return Ok(()) if all validation checks did pass
        Ok(())
    }

    fn value(&self) -> &String {
        &self.0
    }

    fn set_value(&mut self, value: String) -> Result<()> {
        Self::check(&value)?;
        self.0 = value;
        Ok(())
    }
}


#[derive(Debug, Clone, Serialize, Deserialize, AsExpression, FromSqlRow)]
#[sql_type = "Text"]
pub struct Url(String);

try_from_strings!(Url);
from_impl!(Url(String) -> Strings);
from_sql_impl!(Text -> Url(String));
to_sql_impl!(Url(String) -> Text);
display_impl!(Url(String));

impl CheckedType<String> for Url {
    fn check(value: &String) -> Result<()> {
        let chars_len = value.chars().count();

        // check the url length, return error if it is short.
        // if chars_len < 6 {
        //     return Err(ErrorKind::ShortUrl(value.to_owned()))?;
        // }

        if chars_len > 200 {
            return Err(ErrorKind::LongUrl(value.to_owned()))?;
        }

        // check if url is only whitespaces, return error if it is.
        if value.is_whitespace() || value.is_empty() {
            return Err(ErrorKind::EmptyUrl)?;
        }

        if value.contains(char::is_whitespace) {
            return Err(ErrorKind::UrlContainsWhitespace(value.to_owned()))?;
        }

        // return Ok(()) if all validation checks did pass
        Ok(())
    }

    fn value(&self) -> &String {
        &self.0
    }

    fn set_value(&mut self, value: String) -> Result<()> {
        Self::check(&value)?;
        self.0 = value;
        Ok(())
    }
}

impl<'v> FromFormValue<'v> for Url {
    type Error = Error;

    fn from_form_value(value: &'v RawStr) -> Result<Self> {
        let msg = format!("Couldn't parse url '{}' from HTML form", value);
        let value = value.url_decode()
            .chain_err(|| msg)?;
        Self::try_from(value)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, AsExpression, FromSqlRow)]
#[sql_type = "Text"]
pub struct Title(String);

deref_impl!(Title(String));
try_from_strings!(Title);
from_impl!(Title(String) -> Strings);
from_sql_impl!(Text -> Title(String));
to_sql_impl!(Title(String) -> Text);
display_impl!(Title(String));

impl CheckedType<String> for Title {
    fn check(value: &String) -> Result<()> {
        let chars_len = value.chars().count();

        // check the url length, return error if it is short.
        if chars_len < 3 {
            return Err(ErrorKind::ShortTitle(value.to_owned()))?;
        }

        if chars_len > 50 {
            return Err(ErrorKind::LongTitle(value.to_owned()))?;
        }

        // check if url is only whitespaces, return error if it is.
        if value.is_whitespace() || value.is_empty() {
            return Err(ErrorKind::EmptyTitle)?;
        }

        // return Ok(()) if all validation checks did pass
        Ok(())
    }

    fn value(&self) -> &String {
        &self.0
    }

    fn set_value(&mut self, value: String) -> Result<()> {
        Self::check(&value)?;
        self.0 = value;
        Ok(())
    }
}

impl<'v> FromFormValue<'v> for Title {
    type Error = Error;

    fn from_form_value(value: &'v RawStr) -> Result<Self> {
        let msg = format!("Couldn't parse title '{}' from HTML form", value);
        let value = value.url_decode()
            .chain_err(|| msg)?;
        Self::try_from(value)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, AsExpression, FromSqlRow)]
#[sql_type = "Text"]
pub struct Summary(String);

try_from_strings!(Summary);
from_impl!(Summary(String) -> Strings);
from_sql_impl!(Text -> Summary(String));
to_sql_impl!(Summary(String) -> Text);
display_impl!(Summary(String));

impl CheckedType<String> for Summary {
    fn check(value: &String) -> Result<()> {
        let chars_len = value.chars().count();

        // check the length, return error if it is short.
        if chars_len < 3 {
            return Err(ErrorKind::ShortSummary(value.to_owned()))?;
        }

        if chars_len > 150 {
            return Err(ErrorKind::LongSummary(value.to_owned()))?;
        }

        // check if summary is only whitespaces, return error if it is.
        if value.is_whitespace() || value.is_empty() {
            return Err(ErrorKind::EmptySummary)?;
        }

        // check if summary start or end with whitespace, return error if it is.
        if value.starts_with(char::is_whitespace) || value.ends_with(char::is_whitespace) {
            return Err(ErrorKind::SummaryStartOrEndWithWihitespace)?;
        }

        // return Ok(()) if all validation checks did pass
        Ok(())
    }

    fn value(&self) -> &String {
        &self.0
    }

    fn set_value(&mut self, value: String) -> Result<()> {
        Self::check(&value)?;
        self.0 = value;
        Ok(())
    }
}

impl<'v> FromFormValue<'v> for Summary {
    type Error = Error;

    fn from_form_value(value: &'v RawStr) -> Result<Self> {
        let msg = format!("Couldn't parse summary '{}' from HTML form", value);
        let value = value.url_decode()
            .chain_err(|| msg)?;
        Self::try_from(value)
    }
}
