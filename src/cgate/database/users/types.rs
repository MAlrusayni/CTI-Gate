use diesel::sql_types::*;
use errors::*;
use macros::*;
use checked_types::*;
use regex::Regex;
use rand::{OsRng, Rng};
use argon2rs::{self, Argon2};
use std::{
    convert::TryFrom,
    char,
};
use rocket::{
    request::FromFormValue,
    http::{RawStr, Cookie},
};

pub use database::types::{Id, DateTime, PageId};

#[derive(Debug, Clone, Copy, Serialize, Deserialize, AsExpression, FromSqlRow)]
#[sql_type = "Integer"]
pub enum Status {
    Banned,
    Fine,
    Expired,
}

to_sql_impl_for_enum!(Status as i32 -> Integer);
from_sql_impl!(Integer -> Status(i32));

impl Default for Status {
    fn default() -> Self {
        Status::Fine
    }
}

impl TryFrom<i32> for Status {
    type Error = Error;

    fn try_from(value: i32) -> Result<Self> {
        let status = match value {
            0 => Status::Banned,
            1 => Status::Fine,
            2 => Status::Expired,
            _ => return Err(ErrorKind::UnknownUserStatus(value).into()),
        };
        Ok(status)
    }
}

impl From<Status> for i32 {
    fn from(value: Status) -> i32 {
        match value {
            Status::Banned => 0,
            Status::Fine => 1,
            Status::Expired => 2,
        }
    }
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, AsExpression, FromSqlRow, PartialEq)]
#[sql_type = "Integer"]
pub enum Permission {
    Root,
    Admin,
    User,
}

to_sql_impl_for_enum!(Permission as i32 -> Integer);
from_sql_impl!(Integer -> Permission(i32));

impl Default for Permission {
    fn default() -> Self {
        Permission::User
    }
}

impl TryFrom<i32> for Permission {
    type Error = Error;

    fn try_from(value: i32) -> Result<Self> {
        let status = match value {
            0 => Permission::Root,
            10 => Permission::Admin,
            100 => Permission::User,
            _ => return Err(ErrorKind::UnknownUserPermission(value).into()),
        };
        Ok(status)
    }
}

impl From<Permission> for i32 {
    fn from(value: Permission) -> i32 {
        match value {
            Permission::Root => 0,
            Permission::Admin => 10,
            Permission::User => 100,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, AsExpression, FromSqlRow)]
#[sql_type = "Text"]
pub struct Username(String);

deref_impl!(Username(String));
try_from_strings!(Username);
from_impl!(Username(String) -> Strings);
from_sql_impl!(Text -> Username(String));
to_sql_impl!(Username(String) -> Text);
display_impl!(Username(String));

impl CheckedType<String> for Username {
    fn check(value: &String) -> Result<()> {
        let chars_len = value.chars().count();
        // check if username is only whitespaces, return error if it is.
        if value.is_whitespace() || value.is_empty() {
            return Err(ErrorKind::EmptyUsername)?;
        }

        // check the length, return error if it is short.
        if chars_len < 3 {
            return Err(ErrorKind::ShortUsername(value.to_owned()))?;
        }

        // check the length, return error if it is long.
        if chars_len > 25 {
            return Err(ErrorKind::LongUsername(value.to_owned()))?;
        }

        // check if username start or end with whitespace, return error if it is.
        if value.starts_with(char::is_whitespace) || value.ends_with(char::is_whitespace) {
            return Err(ErrorKind::UsernamWithStartOrEndWithWihitespace(value.to_owned()))?;
        }

        // check if username contains any non alphanumeric character, return error it it does.
        let words = value.split_whitespace();
        for word in words {
            if !word.is_alphanumeric() {
                return Err(ErrorKind::UsernameContainNonAlphanumeric(value.to_owned()))?;
            }
        }

        // return Ok(()) if all validation checks did pass
        Ok(())
    }

    fn value(&self) -> &String {
        &self.0
    }

    fn set_value(&mut self, value: String) -> Result<()> {
        Self::check(&value)?;
        self.0 = value;
        Ok(())
    }
}

impl<'v> FromFormValue<'v> for Username {
    type Error = Error;

    fn from_form_value(value: &'v RawStr) -> Result<Self> {
        let msg = format!("Couldn't parse username '{}' from HTML form", value);
        let value = value.url_decode()
            .chain_err(|| msg)?;
        Self::try_from(value)
    }
}

impl From<Username> for Cookie<'static> {
    fn from(username: Username) -> Cookie<'static> {
        Cookie::new("username", username.0)
    }
}

pub type Nickname = Username;

#[derive(Debug, Clone, Serialize, Deserialize, AsExpression, FromSqlRow)]
#[sql_type = "Text"]
pub struct FirstName(String);

deref_impl!(FirstName(String));
try_from_strings!(FirstName);
from_impl!(FirstName(String) -> Strings);
from_sql_impl!(Text -> FirstName(String));
to_sql_impl!(FirstName(String) -> Text);
display_impl!(FirstName(String));

impl CheckedType<String> for FirstName {
    fn check(value: &String) -> Result<()> {
        let chars_len = value.chars().count();

        // check if value is only whitespaces, return error if it is.
        if value.is_empty() || value.is_whitespace() {
            return Err(ErrorKind::EmptyFirstName)?;
        }

        // check the length, return error if it is short.
        if chars_len < 3 {
            return Err(ErrorKind::ShortFirstName(value.to_owned()))?;
        }

        // check the length, return error if it is long.
        if chars_len > 15 {
            return Err(ErrorKind::LongFirstName(value.to_owned()))?;
        }

        // check if value contains whitespace, return error if it is.
        if value.contains(char::is_whitespace) {
            return Err(ErrorKind::FirstNameContainsWhitespace(value.to_owned()))?;
        }

        for character in value.chars() {
            if !character.is_alphabetic() {
                return Err(ErrorKind::FirstNameContainsNonAlphabetic(value.to_owned()))?;
            }
        }

        // return Ok(()) if all validation checks did pass
        Ok(())
    }

    fn value(&self) -> &String {
        &self.0
    }

    fn set_value(&mut self, value: String) -> Result<()> {
        Self::check(&value)?;
        self.0 = value;
        Ok(())
    }
}

impl<'v> FromFormValue<'v> for FirstName {
    type Error = Error;

    fn from_form_value(value: &'v RawStr) -> Result<FirstName> {
        let msg = format!("Couldn't parse Firstname '{}' from HTML form", value);
        let value = value.url_decode()
            .chain_err(|| msg)?;
        Self::try_from(value)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, AsExpression, FromSqlRow)]
#[sql_type = "Text"]
pub struct LastName(String);

deref_impl!(LastName(String));
try_from_strings!(LastName);
from_impl!(LastName(String) -> Strings);
from_sql_impl!(Text -> LastName(String));
to_sql_impl!(LastName(String) -> Text);
display_impl!(LastName(String));

impl CheckedType<String> for LastName {
    fn check(value: &String) -> Result<()> {
        let chars_len = value.chars().count();

        // check if value is only whitespaces, return error if it is.
        if value.is_empty() || value.is_whitespace() {
            return Err(ErrorKind::EmptyLastName)?;
        }

        // check the length, return error if it is short.
        if chars_len < 3 {
            return Err(ErrorKind::ShortLastName(value.to_owned()))?;
        }

        // check the length, return error if it is long.
        if chars_len > 15 {
            return Err(ErrorKind::LongLastName(value.to_owned()))?;
        }

        // check if value contains whitespace, return error if it is.
        if value.contains(char::is_whitespace) {
            return Err(ErrorKind::LastNameContainsWhitespace(value.to_owned()))?;
        }

        for character in value.chars() {
            if !character.is_alphabetic() {
                return Err(ErrorKind::LastNameContainsNonAlphabetic(value.to_owned()))?;
            }
        }

        // return Ok(()) if all validation checks did pass
        Ok(())
    }

    fn value(&self) -> &String {
        &self.0
    }

    fn set_value(&mut self, value: String) -> Result<()> {
        Self::check(&value)?;
        self.0 = value;
        Ok(())
    }
}

impl<'v> FromFormValue<'v> for LastName {
    type Error = Error;

    fn from_form_value(value: &'v RawStr) -> Result<Self> {
        let msg = format!("Couldn't parse Lastname '{}; from HTML form", value);
        let value = value.url_decode()
            .chain_err(|| msg)?;
        Self::try_from(value)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, AsExpression, FromSqlRow)]
#[sql_type = "Text"]
pub struct Email(String);

deref_impl!(Email(String));
try_from_strings!(Email);
from_impl!(Email(String) -> Strings);
from_sql_impl!(Text -> Email(String));
to_sql_impl!(Email(String) -> Text);
display_impl!(Email(String));

impl CheckedType<String> for Email {
    fn check(value: &String) -> Result<()> {
        lazy_static! {
            static ref REG_EMAIL: Regex = Regex::new(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)").unwrap();
        }

        let chars_len = value.chars().count();

        if value.is_empty() || value.is_whitespace() {
            return Err(ErrorKind::EmptyEmail)?;
        }

        if chars_len < 3 {
            return Err(ErrorKind::ShortEmail(value.to_owned()))?;
        }

        if chars_len > 254 {
            return Err(ErrorKind::LongEmail(value.to_owned()))?;
        }

        // check if the input have vaild email pattern
        if !REG_EMAIL.is_match(value) {
            return Err(ErrorKind::EmailDoNotMatchEmailPattern(value.to_owned()))?;
        }

        Ok(())
    }

    fn value(&self) -> &String {
        &self.0
    }

    fn set_value(&mut self, value: String) -> Result<()> {
        Self::check(&value)?;
        self.0 = value;
        Ok(())
    }
}

impl<'v> FromFormValue<'v> for Email {
    type Error = Error;

    fn from_form_value(value: &'v RawStr) -> Result<Email> {
        let msg = format!("Couldn't decode email '{}' from HTML form", value);
        let value = value.url_decode()
            .chain_err(|| msg)?;
        Self::try_from(value)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, AsExpression, FromSqlRow)]
#[sql_type = "Text"]
pub struct PasswordHash(String);

deref_impl!(PasswordHash(String));
try_from_strings!(PasswordHash);
from_impl!(PasswordHash(String) -> Strings);
from_sql_impl!(Text -> PasswordHash(String));
to_sql_impl!(PasswordHash(String) -> Text);
display_impl!(PasswordHash(String));

impl CheckedType<String> for PasswordHash {
    // TODO: add checkes here
    fn check(_value: &String) -> Result<()> {
        Ok(())
    }

    fn value(&self) -> &String {
        &self.0
    }

    fn set_value(&mut self, value: String) -> Result<()> {
        Self::check(&value)?;
        self.0 = value;
        Ok(())
    }
}

impl PasswordHash {
    pub fn new(password: &Password, slat: &Slat) -> Result<Self> {
        let argon2 = Argon2::default(argon2rs::Variant::Argon2i);
        let mut hash = [0u8; 32];
        argon2.hash(
            &mut hash,
            password.as_bytes(),
            slat.as_bytes(),
            b"",
            b"",
        );

        let mut hash_hex = String::from("");
        for byte in hash.iter() {
            hash_hex += &format!("{:02x}", byte);
        }

        Self::try_from(hash_hex)
    }
}


impl From<PasswordHash> for Cookie<'static> {
    fn from(password: PasswordHash) -> Cookie<'static> {
        Cookie::new("password", password.0)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, AsExpression, FromSqlRow)]
#[sql_type = "Text"]
pub struct Password(String);

deref_impl!(Password(String));
try_from_strings!(Password);
from_impl!(Password(String) -> Strings);
from_sql_impl!(Text -> Password(String));
to_sql_impl!(Password(String) -> Text);
display_impl!(Password(String));

impl CheckedType<String> for Password {
    fn check(value: &String) -> Result<()> {
        let chars_len = value.chars().count();

        // check if empty
        if value.is_empty() || value.is_whitespace() {
            return Err(ErrorKind::EmptyPassword)?;
        }

        // check if short
        if chars_len < 8 {
            return Err(ErrorKind::ShortPassword)?;
        }

        // check if long
        if chars_len > 64 {
            return Err(ErrorKind::LongPassword)?;
        }

        // check if follow the password pattern
        let check = value.contains(char::is_alphabetic)
            && value.contains(char::is_numeric);

        if !check {
            return Err(ErrorKind::DoNotMatchPasswordPattern)?;
        }

        // pass checks if there wasn't any error
        Ok(())
    }

    fn value(&self) -> &String {
        &self.0
    }

    fn set_value(&mut self, value: String) -> Result<()> {
        Self::check(&value)?;
        self.0 = value;
        Ok(())
    }
}

impl<'v> FromFormValue<'v> for Password {
    type Error = Error;

    fn from_form_value(value: &'v RawStr) -> Result<Self> {
        let msg = format!("Couldn't parse Password from HTML form");
        let value = value.url_decode()
            .chain_err(|| msg)?;
        Self::try_from(value)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, AsExpression, FromSqlRow)]
#[sql_type = "Text"]
pub struct Slat(String);

deref_impl!(Slat(String));
try_from_strings!(Slat);
from_impl!(Slat(String) -> Strings);
from_sql_impl!(Text -> Slat(String));
to_sql_impl!(Slat(String) -> Text);
display_impl!(Slat(String));

impl CheckedType<String> for Slat {
    fn check(value: &String) -> Result<()> {
        if value.is_empty() || value.is_whitespace() {
            return Err(ErrorKind::EmptySlat)?;
        }

        if value.chars().count() != 64 {
            return Err(ErrorKind::InvalidSlatLength)?;
        }

        Ok(())
    }

    fn value(&self) -> &String {
        &self.0
    }

    fn set_value(&mut self, value: String) -> Result<()> {
        Self::check(&value)?;
        self.0 = value;
        Ok(())
    }
}

impl Slat {
    pub fn random_slat() -> Result<Self> {
        let mut os_rand = OsRng::new()?;
        let mut slat = String::from("");

        for character in os_rand.gen_ascii_chars() {
            slat += &character.to_string();

            if slat.chars().count() == 64 {
                break;
            }
        }

        Self::try_from(slat)
    }
}


#[derive(Debug, Clone, Serialize, Deserialize, AsExpression, FromSqlRow)]
#[sql_type = "Text"]
pub struct Phone(String);

deref_impl!(Phone(String));
try_from_strings!(Phone);
from_impl!(Phone(String) -> Strings);
from_sql_impl!(Text -> Phone(String));
to_sql_impl!(Phone(String) -> Text);
display_impl!(Phone(String));

impl CheckedType<String> for Phone {
    fn check(value: &String) -> Result<()> {
        lazy_static! {
            static ref REG_PHONE: Regex = Regex::new(r"^(05[0-9]{8,12}|٠٥[٠١٢٣٤٥٦٧٨٩]{8,12})$").unwrap();
        }

        let chars_len = value.chars().count();

        if value.is_empty() || value.is_whitespace() {
            return Err(ErrorKind::EmptyPhone)?;
        }

        if chars_len < 9 {
            return Err(ErrorKind::ShortPhone(value.to_owned()))?;
        }

        if chars_len > 14 {
            return Err(ErrorKind::LongPhone(value.to_owned()))?;
        }

        if !REG_PHONE.is_match(value) {
            return Err(ErrorKind::PhoneDoNotMatchPhonePattern(value.clone().into()))?;
        }

        Ok(())
    }

    fn value(&self) -> &String {
        &self.0
    }

    fn set_value(&mut self, value: String) -> Result<()> {
        Phone::check(&value)?;
        self.0 = value;
        Ok(())
    }
}

impl<'v> FromFormValue<'v> for Phone {
    type Error = Error;

    fn from_form_value(value: &'v RawStr) -> Result<Self> {
        let msg = format!("Couldn't parse phone '{}' from HTML form", value);
        let value = value.url_decode()
            .chain_err(|| msg)?;
        Self::try_from(value)
    }
}
