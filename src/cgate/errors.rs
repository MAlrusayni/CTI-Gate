use std::num::ParseIntError;

error_chain! {
    errors {
        // pages::request_guards errors
        UsernameCookieNotFound {
            description("username cookie not found"),
            display("Username cookie not found"),
        }

        PasswordCookieNotFound {
            description("password cookie not found"),
            display("Password cookie not found"),
        }

        // database::users::UserDb errors
        NotAvailableUsername(username: String) {
            description("username is already in use"),
            display("Username '{}' is used, consider using another username", username),
        }

        NotAvailablePageId(page_id: String) {
            description("page id is already in use"),
            display("Page id '{}' is used, consider using another page id", page_id),
        }

        NotAvailablePhone(phone: String) {
            description("phone is already in use"),
            display("Phone '{}' is used, consider using another phone", phone),
        }

        NotAvailableEmail(email: String) {
            description("email is already in use"),
            display("Email '{}' is used, consider using another email", email),
        }

        ///
        NotAvailableTitle(title: String) {
            description("title is already in use"),
            display("Title '{}' is used, consider using another title", title),
        }

        NotAvailableSummary(summary: String) {
            description("summary is already in use"),
            display("Summary '{}' is used, consider using another summary", summary),
        }

        // database::about_us::types errors
        EmptyInfo {
            description("empty info"),
            display("Empty info, consider writing some info"),
        }

        ShortInfo(info: String) {
            description("short info"),
            display("Info '{}' very short, consider using using longer info", info),
        }

        LongInfo(info: String) {
            description("long info"),
            display("Info '{}' very long, consider using using shorter info", info),
        }

        // users::Stauts errors
        UnknownUserStatus(value: i32) {
            description("unknown user status"),
            display("Unknown user status '{}'", value),
        }

        // users::Permission errors
        UnknownUserPermission(value: i32) {
            description("unknown user permission"),
            display("Unknown user permission '{}'", value),
        }

        // common errors
        OverflowCouldHappen {
            description("overflow could happen"),
            display("value reach its maximum limit"),
        }

        //
        UserIdNotFoundInDatabase(user_id: i32) {
            description("user id not found in database"),
            display("User with ID '{}' not found in database", user_id),
        }

        ClubIdNotFoundInDatabase(club_id: i32) {
            description("club id not found in database"),
            display("Club with ID '{}' not found in database", club_id),
        }

        PostIdNotFoundInDatabase(post_id: i32) {
            description("post id not found in database"),
            display("Post with ID '{}' not found in database", post_id),
        }

        CourseIdNotFoundInDatabase(course_id: i32) {
            description("course id not found in database"),
            display("Course with ID '{}' not found in database", course_id),
        }

        NoPostsAvailable {
            description("there is not post in the database"),
            display("There is no post registered in the database"),
        }

        NoClubsAvailable {
            description("there is not clubs in the database"),
            display("There is no clubs registered in the database"),
        }

        NoCoursesAvailable {
            description("there is not courses in the database"),
            display("There is no courses registered in the database"),
        }

        PageNotFound(url: String) {
            description("page not found"),
            display("Page '{}' not found", url),
        }

        OutOfPaginationRange(min: u32, max: u32, page: u32) {
            description("out of pagination range"),
            display("Page '{}' is out pagination range: {}..{}", page, min, max),
        }

        InvalidFromFormValue(msg: &'static str) {
            description("Invalid from form value"),
            display("Invalid input: {}", msg),
        }

        InvalidId(err: ParseIntError) {
            description("invalid id"),
            display("Invalid ID, becuse of: {}", err),
        }

        // models::types::Password errors
        ShortPassword {
            description("short password"),
            display("Password is very short, consider using longer password, atless 8 characters"),
        }

        LongPassword {
            description("long password"),
            display("Password is very long, consider using shorter password, something less than 65 characters"),
        }

        EmptyPassword {
            description("empty password"),
            display("Password cannot be empty"),
        }

        DoNotMatchPasswordPattern {
            description("do not match password pattern"),
            display("Password is not strong, consider using atless one number and one character"),
        }

        // database::users::Slat errors
        InvalidSlatLength {
            description("invalid length for slat"),
            display("Slat must contain 64 characters"),
        }

        EmptySlat {
            description("empty slat"),
            display("Slat cannot be empty"),
        }


        // models::types::Email errors
        ShortEmail(email: String) {
            description("short email"),
            display("Email '{}' is very short, consider using longer email", email),
        }

        LongEmail(email: String) {
            description("long email"),
            display("Email '{}' is very long, consider using shorter email", email),
        }

        EmptyEmail {
            description("empty email"),
            display("Email cannot be empty"),
        }

        EmailDoNotMatchEmailPattern(email: String) {
            description("donot match emails pattern"),
            display("Email '{}' doesn't looks like valid email, a valid email would look like this 'name@exampl.com'", email),
        }

        // models::types::Username errors
        ShortUsername(username: String) {
            description("short username"),
            display("Username '{}' is very short, consider using longer username", username),
        }

        LongUsername(username: String) {
            description("long username"),
            display("Username '{}' is very long, consider using shorter username", username),
        }

        UsernamWithStartOrEndWithWihitespace(username: String) {
            description("username should not begin or end with whitespace"),
            display("Username '{}' cannot begin or end with whitespace, consider removing leading and trailing whitespaces", username),
        }

        EmptyUsername {
            description("empty username"),
            display("Username cannot be empty, atless 3 characters should be written"),
        }

        UsernameContainNonAlphanumeric(username: String) {
            description("username should only contain alphanumeric and whitespaces characters"),
            display("Username '{}' should only contain alphanumeric and whitespaces characters", username),
        }

        // models::types::FirstNmae errors
        ShortFirstName(name: String) {
            description("short first name"),
            display("First name '{}' is very short, consider using longer first name", name),
        }

        LongFirstName(name: String) {
            description("long first name"),
            display("Last name '{}' is very long, consider using shorter first name", name),
        }

        FirstNameContainsWhitespace(name: String) {
            description("first name contains whitespace"),
            display("First name '{}' cannot contains whitespace", name),
        }

        EmptyFirstName {
            description("empty first name"),
            display("First name cannot be empty"),
        }

        FirstNameContainsNonAlphabetic(name: String) {
            description("first name should only contain alphabetic characters"),
            display("First name '{}' should only contain alphabetic characters", name),
        }

        // models::types::LastNmae errors
        ShortLastName(name: String) {
            description("short last name"),
            display("Last name '{}' is very short, consider using longer last name", name),
        }

        LongLastName(name: String) {
            description("long last name"),
            display("Last name '{}' is very long, consider using shorter last name", name),
        }

        LastNameContainsWhitespace(name: String) {
            description("last name contains whitespace"),
            display("Last name '{}' cannot contains whitespace", name),
        }

        EmptyLastName {
            description("empty last name"),
            display("Last name cannot be empty"),
        }

        LastNameContainsNonAlphabetic(name: String) {
            description("last name should only contain alphabetic characters"),
            display("Last name '{}' should only contain alphabetic characters", name),
        }

        // models::types::Phone errors
        ShortPhone(phone: String) {
            description("short phone"),
            display("Phone '{}' is very short, consider using longer phone number", phone),
        }

        LongPhone(phone: String) {
            description("long phone"),
            display("Phone '{}' is very long, consider using shorter phone", phone),
        }

        PhoneDoNotMatchPhonePattern(phone: String) {
            description("do not match phone pattern"),
            display("Phone '{}' doesn't look like phone number, a valid phone would look like this '0504567890'", phone),
        }

        EmptyPhone {
            description("empty phone"),
            display("Phone can not be empty"),
        }

        // database::contact_us_messages::Status errors
        UnknownContactUsMessageStatus(status: i32) {
            description("unknown status value"),
            display("'{}' is unknown status", status),
        }

        // database::contact_us_messages::Category errors
        UnknownContactUsMessageCategory(value: i32) {
            description("unknow category value"),
            display("'{}' is unknow category", value),
        }

        // database::contact_us_messages::Subject errors
        LongSubject(subject: String) {
            description("long subject"),
            display("'{}' is very long subject, consider using shorte subject", subject),
        }

        EmptySubject {
            description("subject can not be whitespaces or empty"),
            display("subject cannot be empty"),
        }

        SubjectStartsOrEndWithWhitespace(subject: String) {
            description("subject can not starts or ends with whitespace"),
            display("'{}' subject can not starts or ends with whitespace, consider removing leading and trailing whitespaces", subject),
        }

        // database::contact_us_messages::Content errors
        LongContent(content: String) {
            description("long content"),
            display("'{}' is very long content, consider using shorte content", content),
        }

        EmptyContent {
            description("empty content"),
            display("Content cannot be empty"),
        }

        ContentStartsOrEndWithWhitespace(content: String) {
            description("content can not starts or ends with whitespace"),
            display("'{}' content can not starts or ends with whitespace, consider removing leading and trailing whitespaces", content),
        }

        // database::types::Url errors
        EmptyUrl {
            description("empty url"),
            display("Url cannot be empty"),
        }

        ShortUrl(url: String) {
            description("short urpl"),
            display("Url '{}' is very short, consider using longer url", url),
        }

        LongUrl(url: String) {
            description("long url"),
            display("Url '{}' is very long, consider using shorter url", url),
        }

        UrlContainsWhitespace(url: String) {
            description("url contains whitespace"),
            display("Url '{}' cannot contain whitespace, consider removing them", url),
        }

        // database::types::Title errors
        EmptyTitle {
            description("empty title"),
            display("Title cannot be empty"),
        }

        ShortTitle(title: String) {
            description("short title"),
            display("Title '{}' is very short, consider using longer title", title),
        }

        LongTitle(title: String) {
            description("long title"),
            display("Title '{}' is very long, consider using shorter title", title),
        }

        // database::types::PageId errors
        EmptyPageId {
            description("page id can not be whitespaces or empty"),
            display("Page id should have some characters"),
        }

        LongPageId(page_id: String) {
            description("long page id"),
            display("Page id '{}' is very long content, consider using shorte content", page_id),
        }

        PageIdContainsWhitespace(page_id: String) {
            description("page id contains whitespace"),
            display("Page id '{}' cannot contains whitespace", page_id),
        }

        PageIdContainsNotAllowedCharacter(page_id: String) {
            description("page id contains not allowed characterr"),
            display("Page id '{}' contains not aloowed characters, page id should consist only from 'a..z', '0..9', '-' and '_' characters", page_id),
        }

        // database::clubs::types::ClubName errors
        LongClubName(name: String) {
            description("long club name"),
            display("Club name '{}' is very long, consider using shorte club name", name),
        }

        ShortClubName(name: String) {
            description("short club name"),
            display("Club name '{}' is very short, consider using longer club name", name),
        }

        EmptyClubName {
            description("club name can not be whitespaces or empty"),
            display("Club name should have some characters"),
        }

        ClubNameStartOrEndWithWihitespace(name: String) {
            description("club name should not begin or end with whitespace"),
            display("Club name '{}' should not begin or end with whitespace, consider removing leading and trailing whitespaces", name),
        }

        ClubNameContainNonAlphanumeric(name: String) {
            description("club name should only contain alphanumeric and whitespaces characters"),
            display("Club name '{}' should only contain alphanumeric and whitespaces characters", name),
        }

        // database::posts::types::Summary errors
        ShortSummary(summary: String) {
            description("short summary"),
            display("Summary name '{}' is very short, consider using longer summary", summary),
        }

        LongSummary(summary: String) {
            description("long summary"),
            display("Summary name '{}' is very long, consider using shorter summary ", summary),
        }

        EmptySummary {
            description("empty summary"),
            display("Summary should have some characters"),
        }

        SummaryStartOrEndWithWihitespace {
            description("summary start or end with whitespace"),
            display("Summary should not begin or end with whitespace, consider removing leading and trailing whitespaces"),
        }

        // database::types::PageContent errors
        ShortPageContent(page_content: String) {
            description("short page content"),
            display("Page content name '{}' is very short, consider using longer page content", page_content),
        }

        EmptyPageContent {
            description("empty page content"),
            display("Page content should contain a few lines"),
        }



        // database::types::Id errors
        NegativeId(id: i32) {
            description("negative id"),
            display("ID '{}' cannot be negative, make sure its value equal 0 or above", id),
        }

        // database::types::Description errors
        EmptyDescription {
            description("description can not be empty"),
            display("description should contain a few characters, atless 10"),
        }

        LongDescription {
            description("long description"),
            display("Description is very long, make sure it's less that 300 characters"),
        }

        ShortDescription(description: String) {
            description("short description"),
            display("Description '{}' is very short, make sure it's more that 10 characters", description),
        }

        // database::contact_us_messages::ContactUsMessagesdb errors
        IncompleteMessageForm(reason: String) {
            description("incomplete message form"),
            display("Incomplete message form, becuse: '{}'", reason),
        }

        // database::users::SignUpForm errors
        IncompleteSignUpForm(reason: String) {
            description("incomplete signup form"),
            display("Incomplete sign-up form, becuse: '{}'", reason),
        }

        // database::users::LoginForm errors
        IncompleteLoginForm(reason: String) {
            description("incomplete login form"),
            display("Incomplete login form, becuse: '{}'", reason),
        }

        //
        IncompleteNewPostForm(reason: String) {
            description("incomplete new post form"),
            display("Incomplete new post form, becuse: '{}'", reason),
        }

        //
        IncompleteEditPostForm(reason: String) {
            description("incomplete edit post form"),
            display("Incomplete edit post form, becuse: '{}'", reason),
        }
    }

    foreign_links {
        Database(::r2d2::Error);
        Diesel(::diesel::result::Error);
        Utf8(::std::str::Utf8Error);
        ParseInt(::std::num::ParseIntError);
        Io(::std::io::Error);
        Argon2(::argon2rs::ParamErr);
    }
}
