#![feature(decl_macro)]
#![feature(plugin)]
#![plugin(rocket_codegen)]
#![feature(custom_derive)]
#![recursion_limit = "1024"]

extern crate rocket;
extern crate rocket_contrib;

#[macro_use]
extern crate error_chain;

#[macro_use]
extern crate diesel;
extern crate r2d2_diesel;
extern crate r2d2;

#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;

extern crate chrono;
extern crate num_traits;
extern crate regex;

#[macro_use]
extern crate lazy_static;

extern crate argon2rs;
extern crate rand;

use errors::*;
use rocket::{
    response::NamedFile,
    Route
};
use rocket_contrib::Template;
use std::path::{Path, PathBuf};

pub mod pages;
pub mod database;
pub mod contexts;
pub mod errors;
pub mod macros;
pub mod checked_types;

#[get("/theme/css/<file..>")]
fn theme_css_folder(file: PathBuf) -> Option<NamedFile> {
    let path = Path::new("resources/theme/css/").join(file);
    NamedFile::open(path).ok()
}

#[get("/theme/fonts/<file..>")]
fn theme_fonts_folder(file: PathBuf) -> Option<NamedFile> {
    let path = Path::new("resources/theme/fonts/").join(file);
    NamedFile::open(path).ok()
}

#[get("/imgs/<file..>")]
fn imgs_folder(file: PathBuf) -> Option<NamedFile> {
    let path = Path::new("resources/images/").join(file);
    NamedFile::open(path).ok()
}

pub fn run() -> Result<()> {
    let mut routes: Vec<Route> = vec![];

    // add pages routes
    // /news
    routes.append(&mut pages::news::news_routes());
    // /new-member
    routes.append(&mut pages::new_member::signup_routes());
    // /login
    routes.append(&mut pages::login::login_routes());
    // /
    routes.append(&mut pages::home::home_routes());
    // /courses
    routes.append(&mut pages::courses::courses_routes());
    // /contact-us
    routes.append(&mut pages::contact_us::contact_us_routes());
    // /clubs
    routes.append(&mut pages::clubs::clubs_routes());
    // /about-us
    routes.append(&mut pages::about_us::about_us_routes());
    // /logout
    routes.append(&mut pages::logout::logout_routes());
    // /memeber/...
    routes.append(&mut pages::member::member_routes());
    // /dashboard
    routes.append(&mut pages::dashboard::dashboard_routes());

    // add resoures routes
    // /imgs/...
    routes.append(&mut routes![imgs_folder]);
    // /theme/css/...
    routes.append(&mut routes![theme_css_folder]);
    // /theme/fonts/...
    routes.append(&mut routes![theme_fonts_folder]);

    rocket::ignite()
        .manage(database::init_db_pool()?)
        .mount("/", routes)
        .attach(Template::fairing())
        .launch();

    Ok(())
}
