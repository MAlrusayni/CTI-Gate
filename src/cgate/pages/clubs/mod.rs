use rocket_contrib::Template;
use pages::Pagination;
use contexts::Link;
use errors::*;
use database::{
    DbConn,
    DatabaseConnection,
    users::{User, UserContext},
    clubs::{ClubContext, ClubsDb},
};
use rocket::{
    Route,
    request::Request,
    response::{self, Responder},
    http::Status,
};

#[derive(Serialize)]
pub struct PageContext {
    phone_buttons: Vec<Link>,
    phone_links: Vec<Link>,
    links: Vec<Link>,
    clubs: Vec<ClubContext>,
    pagination: Vec<Link>,
    user: Option<UserContext>,
}

pub struct ClubsPage {
    conn: DbConn,
    current_page: u32,
    user: Option<User>,
}

impl<'r> ClubsPage {
    fn new(conn: DbConn, current_page: u32, user: Option<User>) -> Self {
        Self {
            conn,
            current_page,
            user,
        }
    }

    fn get_context(&mut self) -> PageContext {
        // get links for the sidebar view
        let mut links = super::get_site_links();
        links[2].to_active();
        let phone_buttons: Vec<Link> = links.iter()
            .take(2)
            .map(|link| link.clone())
            .collect();
        let phone_links: Vec<Link> = links.iter()
            .skip(2)
            .map(|link| link.clone())
            .collect();

        PageContext {
            phone_buttons: phone_buttons,
            phone_links: phone_links,
            links: links,
            clubs: vec![],
            pagination: vec![],
            user: None,
        }
    }

    fn index_handler(&mut self, req: &Request) -> response::Result<'r> {
        let mut context = self.get_context();
        // fetch posts from database and convert them into Vec<PostContext>
        let clubs = self.get_clubs_context()
            .map_err(|_| Status::InternalServerError)?;

        // convert pagination from Vec<u32> into Vec<Link>
        let mut pagination = self.pagination_links("/clubs/page/")
            .map_err(|_| Status::InternalServerError)?;
        // revers the numbers so it feels like arabic language RTL
        pagination.reverse();

        // user
        let user_context = self.user.clone().map(|user| user.into());

        context.clubs = clubs;
        context.pagination = pagination;
        context.user = user_context;

        let template = Template::render("clubs/index", context);
        template.respond_to(req)
    }
}

impl DatabaseConnection for ClubsPage {
    fn db_connection(&self) -> &DbConn {
        &self.conn
    }
}

impl Pagination for ClubsPage {
    fn items_per_page(&self) -> u32 {
        5
    }

    fn pagination_level(&self) -> u32 {
        6
    }

    fn all_items(&self) -> Result<u32> {
        self.get_clubs_number()
    }

    fn page(&self) -> u32 {
        self.current_page
    }

    fn set_page(&mut self, page: u32) {
        self.current_page = page;
    }
}

impl<'r> Responder<'r> for ClubsPage {
    fn respond_to(mut self, req: &Request) -> response::Result<'r> {
        self.index_handler(req)
    }
}

impl ClubsDb for ClubsPage { }

pub fn clubs_routes() -> Vec<Route> {
    routes![clubs, clubs_page_number]
}

#[get("/clubs")]
pub fn clubs(conn: DbConn, user: Option<User>) -> ClubsPage {
    ClubsPage::new(conn, 1, user)
}

#[get("/clubs/page/<page>")]
pub fn clubs_page_number(conn: DbConn, page: u32, user: Option<User>) -> ClubsPage {
    ClubsPage::new(conn, page, user)
}
