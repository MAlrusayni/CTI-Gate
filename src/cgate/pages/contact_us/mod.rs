use errors::*;
use rocket_contrib::Template;
use contexts::Link;
use database::contact_us_messages::types::*;
use database::{
    DbConn,
    DatabaseConnection,
    users::{User, UserContext},
    contact_us_messages::ContactUsMessagesDb,
};
use rocket::{
    Route,
    request::Request,
    response::{self, Redirect, Responder},
    request::Form,
};

#[derive(FromForm, Debug)]
pub struct MessageForm {
    pub nickname: Result<Username>,
    pub email: Result<Email>,
    pub phone: Option<Result<Phone>>,
    pub category: Result<Category>,
    pub subject: Result<Subject>,
    pub content: Result<Content>,
}

#[derive(Serialize)]
pub struct PageContext {
    phone_buttons: Vec<Link>,
    phone_links: Vec<Link>,
    links: Vec<Link>,
    show_success_message: bool,
    other_err: Option<String>,
    username_err: Option<String>,
    email_err: Option<String>,
    phone_err: Option<String>,
    category_err: Option<String>,
    subject_err: Option<String>,
    content_err: Option<String>,
    user: Option<UserContext>
}

impl PageContext {
    pub fn is_message_form_invalid(&self) -> bool {
        self.username_err.is_some() || self.email_err.is_some() ||
            self.phone_err.is_some() || self.category_err.is_some() ||
            self.subject_err.is_some() || self.content_err.is_some()
    }

    fn find_assign_message_form_errors(&mut self, message: &MessageForm) -> bool {
        let mut result = false;
        {
            let mut to_str = |err: &Error| -> Option<String> {
                result = true;
                Some(format!("{}", err))
            };

            if let Err(ref err) = message.nickname { self.username_err = to_str(err) }
            if let Err(ref err) = message.email { self.email_err = to_str(err) }
            if let Err(ref err) = message.category { self.category_err = to_str(err) }
            if let Err(ref err) = message.subject { self.subject_err = to_str(err) }
            if let Err(ref err) = message.content { self.content_err = to_str(err) }

            if let Some(ref phone) = message.phone {
                if let Err(ref err) = phone {
                    self.phone_err = to_str(err)
                }
            }
        }

        result
    }
}

pub struct ContactUsPage {
    conn: DbConn,
    received_message: Option<MessageForm>,
    user: Option<User>,
}

impl<'r> ContactUsPage {
    fn new(
        conn: DbConn,
        received_message: Option<MessageForm>,
        user: Option<User>
    ) -> Self {
        Self {
            conn,
            received_message,
            user,
        }
    }

    fn get_context(&mut self) -> PageContext {
        // get links for the sidebar view
        let mut links = super::get_site_links();
        links[5].to_active();
        let phone_buttons: Vec<Link> = links.iter()
            .take(2)
            .map(|link| link.clone())
            .collect();
        let phone_links: Vec<Link> = links.iter()
            .skip(2)
            .map(|link| link.clone())
            .collect();

        PageContext {
            phone_buttons: phone_buttons,
            phone_links: phone_links,
            links: links,
            show_success_message: false,
            other_err: None,
            username_err: None,
            email_err: None,
            phone_err: None,
            category_err: None,
            subject_err: None,
            content_err: None,
            user: None,
        }
    }

    fn index_handler(&mut self, req: &Request) -> response::Result<'r> {
        let mut  context = self.get_context();
        let user = self.user.clone().map(|user| user.into());
        context.user = user;
        Template::render("contact_us/index", context).respond_to(req)
    }

    fn message_handler(&mut self, req: &Request) -> response::Result<'r> {
        if self.received_message.is_none() {
            // redirecting to index_handler() if there was no message form
            return Redirect::to("/contact-us").respond_to(req);
        }

        let mut context = self.get_context();
        // user info
        let user = self.user.clone().map(|user| user.into());
        context.user = user;
        // unwarp is fine here, since we already checked for none
        let message = self.received_message.as_ref().unwrap();

        // check if message form fields have any error, return with error messages
        // if so.
        if context.find_assign_message_form_errors(message) {
            return Template::render("contact_us/index", context).respond_to(req);
        }

        match self.add_message(message) {
            Ok(_) => context.show_success_message = true,
            Err(ref _err) => context.other_err =
                Some("حصلة مشكلة أثناء إرسال رسالتك، راجع الإدارة إذا تكررت ذه المشكلة".into()),
        }

        Template::render("contact_us/index", context).respond_to(req)
    }
}

impl<'r> Responder<'r> for ContactUsPage {
    fn respond_to(mut self, req: &Request) -> response::Result<'r> {
        if self.received_message.is_none() {
            return self.index_handler(req);
        }
        self.message_handler(req)
    }
}

impl DatabaseConnection for ContactUsPage {
    fn db_connection(&self) -> &DbConn {
        &self.conn
    }
}

impl ContactUsMessagesDb for ContactUsPage { }

pub fn contact_us_routes() -> Vec<Route> {
    routes![contact_us, add_message]
}

#[get("/contact-us")]
pub fn contact_us(conn: DbConn, user: Option<User>) -> ContactUsPage {
    ContactUsPage::new(conn, None, user)
}

#[post("/contact-us", data = "<message>")]
pub fn add_message(
    conn: DbConn,
    message: Option<Form<MessageForm>>,
    user: Option<User>
) -> ContactUsPage {
    let message = message.map(|msg| msg.into_inner());
    ContactUsPage::new(conn, message, user)
}
