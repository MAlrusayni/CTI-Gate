use errors::*;
use contexts::Link;
use rocket_contrib::Template;
use pages::Pagination;
use database::{
    DbConn,
    DatabaseConnection,
    users::{User, UserContext},
    courses::{CourseContext, CoursesDb},
};
use rocket::{
    Route,
    request::Request,
    response::{self, Responder},
    http::Status,
};

#[derive(Serialize)]
pub struct PageContext {
    phone_buttons: Vec<Link>,
    phone_links: Vec<Link>,
    links: Vec<Link>,
    courses: Vec<CourseContext>,
    pagination: Vec<Link>,
    user: Option<UserContext>,
}

pub struct CoursesPage {
    conn: DbConn,
    current_page: u32,
    user: Option<User>,
}

impl<'r> CoursesPage {
    fn new(conn: DbConn, current_page: u32, user: Option<User>) -> Self {
        Self {
            conn,
            current_page,
            user,
        }
    }

    fn get_context(&mut self) -> PageContext {
        // get links for the sidebar view
        let mut links = super::get_site_links();
        links[3].to_active();
        let phone_buttons: Vec<Link> = links.iter()
            .take(2)
            .map(|link| link.clone())
            .collect();
        let phone_links: Vec<Link> = links.iter()
            .skip(2)
            .map(|link| link.clone())
            .collect();

        PageContext {
            phone_buttons: phone_buttons,
            phone_links: phone_links,
            links: links,
            courses: vec![],
            pagination: vec![],
            user: None,
        }
    }

    fn index_handler(&mut self, req: &Request) -> response::Result<'r> {
        let mut context = self.get_context();
        // fetch courses from database
        let courses = self.get_courses_context()
            .map_err(|_| Status::InternalServerError)?;
        // get pagination as Vec<Link>
        let mut pagination = self.pagination_links("/courses/page/")
            .map_err(|_| Status::InternalServerError)?;
        // revers the numbers so it feels like arabic language RTL
        pagination.reverse();

        // user
        let user_context = self.user.clone().map(|user| user.into());

        context.courses = courses;
        context.pagination = pagination;
        context.user = user_context;

        Template::render("courses/index", context).respond_to(req)
    }
}

impl DatabaseConnection for CoursesPage {
    fn db_connection(&self) -> &DbConn {
        &self.conn
    }
}

impl Pagination for CoursesPage {
    fn items_per_page(&self) -> u32 {
        2
    }

    fn pagination_level(&self) -> u32 {
        6
    }

    fn all_items(&self) -> Result<u32> {
        self.get_courses_number()
    }

    fn page(&self) -> u32 {
        self.current_page
    }

    fn set_page(&mut self, page: u32) {
        self.current_page = page;
    }
}

impl<'r> Responder<'r> for CoursesPage {
    fn respond_to(mut self, req: &Request) -> response::Result<'r> {
        self.index_handler(req)
    }
}

impl CoursesDb for CoursesPage { }

pub fn courses_routes() -> Vec<Route> {
    routes![courses, courses_page_number]
}

#[get("/courses")]
pub fn courses(conn: DbConn, user: Option<User>) -> CoursesPage {
    CoursesPage::new(conn, 1, user)
}

#[get("/courses/page/<page>")]
pub fn courses_page_number(conn: DbConn, page: u32, user: Option<User>) -> CoursesPage {
    CoursesPage::new(conn, page, user)
}
