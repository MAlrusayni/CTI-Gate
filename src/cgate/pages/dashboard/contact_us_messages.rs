use errors::*;
use rocket_contrib::Template;
use pages::Pagination;
use pages::get_site_links;
use rocket::{
    Route,
    http::Status,
    request::{Request, Form, FlashMessage},
    response::{self, Redirect, Responder, Flash},
};
use contexts::Link;
use database::{
    DbConn,
    DatabaseConnection,
    about_us::AboutUsDb,
    clubs::ClubsDb,
    contact_us_messages::{ContactUsMessagesDb, ContactUsMessage},
    courses::CoursesDb,
    posts::{PostContext, PostsDb},
    services::ServicesDb,
    users::{
        User,
        UserContext,
        UsersDb,
        types::*,
    },
};

#[derive(Debug, Serialize)]
pub struct PageContext {
    phone_buttons: Vec<Link>,
    phone_links: Vec<Link>,
    links: Vec<Link>,
    messages: Vec<ContactUsMessage>,
    pagination: Vec<Link>,
    user: UserContext,
    redirect_message: Option<String>,
}

pub struct ContactUsMessagesPage {
    conn: DbConn,
    current_page: u32,
    admin: User,
    redirect_message: Option<String>,
}

impl<'r> ContactUsMessagesPage {
    fn new(
        conn: DbConn,
        current_page: u32,
        admin: User,
        redirect_message: Option<String>,
    ) -> Self {
        Self {
            conn,
            current_page,
            admin,
            redirect_message,
        }
    }

    fn get_context(&mut self) -> PageContext {
        // get links for the sidebar view
        let links = get_site_links();
        let phone_buttons: Vec<Link> = links.iter()
            .take(2)
            .map(|link| link.clone())
            .collect();
        let phone_links: Vec<Link> = links.iter()
            .skip(2)
            .map(|link| link.clone())
            .collect();

        PageContext {
            phone_buttons: phone_buttons,
            phone_links: phone_links,
            links: links,
            messages: vec![],
            pagination: vec![],
            user: self.admin.clone().into(),
            redirect_message: self.redirect_message.clone(),
        }
    }

    fn index_handler(&mut self, req: &Request) -> response::Result<'r> {
        let mut context = self.get_context();

        context.messages = self.get_messages_for_current_page()
            .map_err(|_| Status::InternalServerError)?;

        // convert pagination from Vec<u32> into Vec<Link>
        let mut pagination = self.pagination_links("/dashboard/contact-us-messages/")
            .map_err(|_| Status::InternalServerError)?;
        // revers the numbers so it feels like arabic language RTL
        pagination.reverse();
        context.pagination = pagination;

        Template::render("dashboard/contact-us-messages", context).respond_to(req)
    }
}

impl<'r> Responder<'r> for ContactUsMessagesPage {
    fn respond_to(mut self, req: &Request) -> response::Result<'r> {
        if !self.admin.is_admin() {
            return Err(Status::NotFound);
        }
        self.index_handler(req)
    }
}

impl UsersDb for ContactUsMessagesPage { }
impl ContactUsMessagesDb for ContactUsMessagesPage { }

impl DatabaseConnection for ContactUsMessagesPage {
    fn db_connection(&self) -> &DbConn {
        &self.conn
    }
}

impl Pagination for ContactUsMessagesPage {
    fn items_per_page(&self) -> u32 {
        5
    }

    fn pagination_level(&self) -> u32 {
        6
    }

    fn all_items(&self) -> Result<u32> {
        self.get_all_contact_us_messages_numbers()
    }

    fn page(&self) -> u32 {
        self.current_page
    }

    fn set_page(&mut self, page: u32) {
        self.current_page = page;
    }
}

#[get("/dashboard/contact-us-messages/<page>")]
pub fn d_contact_us_messages(
    conn: DbConn,
    admin: User,
    page: u32,
    flash: Option<FlashMessage>
) -> ContactUsMessagesPage {
    ContactUsMessagesPage::new(
        conn,
        page,
        admin,
        flash.map(|msg| format!("{}: {}", msg.name(), msg.msg()))
    )
}

pub struct DeleteMessagePage {
    pub conn: DbConn,
    pub admin: User,
    pub message_id: Id,
}

impl UsersDb for DeleteMessagePage { }
impl ContactUsMessagesDb for DeleteMessagePage { }

impl DatabaseConnection for DeleteMessagePage {
    fn db_connection(&self) -> &DbConn {
        &self.conn
    }
}

impl<'r> Responder<'r> for DeleteMessagePage {
    fn respond_to(mut self, req: &Request) -> response::Result<'r> {
        if !self.admin.is_admin() {
            return Err(Status::NotFound);
        }

        match self.delete_message(&self.message_id) {
            Ok(_) => {
                Flash::new(
                    Redirect::to("/dashboard/contact-us-messages/1"),
                    "تمت العملية",
                    format!("تم حذف الرسالة {} من النظام", self.message_id)
                ).respond_to(req)
            },
            Err(ref err) => {
                Flash::new(
                    Redirect::to("/dashboard/contact-us-messages/1"),
                    "لم تتم العملية",
                    format!("حصل خطأ أثناء حذف الرسالة {}، بسبب: {}", self.message_id, err)
                ).respond_to(req)
            },
        }
    }
}

#[get("/dashboard/contact-us-messages/delete/<message_id>")]
pub fn d_contact_us_messages_delete_message(
    conn: DbConn,
    admin: User,
    message_id: Id,
) -> DeleteMessagePage {
    DeleteMessagePage {
        conn,
        admin,
        message_id,
    }
}

pub fn d_contact_us_messages_routes() -> Vec<Route> {
    routes![d_contact_us_messages, d_contact_us_messages_delete_message]
}
