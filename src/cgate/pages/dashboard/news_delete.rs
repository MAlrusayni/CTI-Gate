use errors::*;
use rocket_contrib::Template;
use pages::get_site_links;
use rocket::{
    Route,
    http::Status,
    request::{Request, Form},
    response::{self, Redirect, Responder},
};
use contexts::Link;
use database::{
    DbConn,
    DatabaseConnection,
    about_us::AboutUsDb,
    clubs::ClubsDb,
    contact_us_messages::ContactUsMessagesDb,
    courses::CoursesDb,
    types::{Title, Summary, PageContent, PageId},
    posts::{PostContext, PostsDb},
    services::ServicesDb,
    users::{
        User,
        UserContext,
        UsersDb,
        types::{Username, FirstName, LastName, Phone, Email, Password},
    },
};

#[derive(Debug, FromForm)]
pub struct NewPostForm {
    pub title: Result<Title>,
    pub summary: Result<Summary>,
    pub page_content: Result<PageContent>,
    pub draft: bool,
}

impl NewPostForm {
    pub fn ok_or_incomplete_form<T: Clone>(value: &Result<T>) -> Result<T> {
        match value {
            Ok(ref value) => Ok(value.clone()),
            Err(ref err) => {
                let reason = format!("{}", err);
                Err(ErrorKind::IncompleteNewPostForm(reason))?
            },
        }
    }
}

#[derive(Debug, Serialize)]
pub struct PageContext {
    phone_buttons: Vec<Link>,
    phone_links: Vec<Link>,
    links: Vec<Link>,
    user: UserContext,
    show_success_message: bool,
    other_err: Option<String>,
    title_err: Option<String>,
    summary_err: Option<String>,
    page_content_err: Option<String>,
}

impl PageContext {
    pub fn is_new_post_form_invalid(&self) -> bool {
        self.title_err.is_some() || self.summary_err.is_some() ||
            self.page_content_err.is_some()
    }

    pub fn find_new_post_from_errors(&mut self, new_post: &NewPostForm) {
        let to_str = |err: &Error| -> Option<String> {
            Some(format!("{}", err))
        };

        if let Err(ref err) = new_post.title { self.title_err = to_str(err) }
        if let Err(ref err) = new_post.summary { self.summary_err = to_str(err) }
        if let Err(ref err) =
            new_post.page_content { self.page_content_err = to_str(err) }
    }
}

pub struct DNewsAddPage {
    conn: DbConn,
    admin: User,
    page_id: PageId,
}

impl<'r> DNewsAddPage {
    fn new(conn: DbConn, admin: User, page_id: PageId) -> Self {
        Self {
            conn,
            admin,
            page_id,
        }
    }

    fn delete_post_handler(&mut self, req: &Request) -> response::Result<'r> {
        match self.delete_post(&self.page_id) {
            Ok(_) => println!("post {} has been deleted", self.page_id),
            Err(ref err) => println!("couldn't delete post {}, becuse {}", self.page_id, err),
        };

        Redirect::to("/dashboard/news/1").respond_to(req)
    }
}

impl<'r> Responder<'r> for DNewsAddPage {
    fn respond_to(mut self, req: &Request) -> response::Result<'r> {
        if !self.admin.is_admin() {
            return Err(Status::NotFound);
        }

        self.delete_post_handler(req)
    }
}

impl UsersDb for DNewsAddPage { }
impl PostsDb for DNewsAddPage { }

impl DatabaseConnection for DNewsAddPage {
    fn db_connection(&self) -> &DbConn {
        &self.conn
    }
}

pub fn d_news_delete_routes() -> Vec<Route> {
    routes![d_news_delete_post]
}

#[get("/dashboard/news/delete/<page_id>")]
pub fn d_news_delete_post(
    conn: DbConn,
    admin: User,
    page_id: PageId,
) -> DNewsAddPage {
    DNewsAddPage::new(conn, admin, page_id)
}
