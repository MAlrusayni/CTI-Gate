use errors::*;
use rocket_contrib::Template;
use pages::Pagination;
use pages::get_site_links;
use rocket::{
    Route,
    http::Status,
    request::{Request, Form, FlashMessage},
    response::{self, Redirect, Responder, Flash},
};
use contexts::Link;
use database::{
    DbConn,
    DatabaseConnection,
    about_us::AboutUsDb,
    clubs::ClubsDb,
    contact_us_messages::ContactUsMessagesDb,
    courses::CoursesDb,
    posts::{PostContext, PostsDb},
    services::ServicesDb,
    users::{
        User,
        UserContext,
        UsersDb,
        types::*,
    },
};

#[derive(Debug, Serialize)]
pub struct PageContext {
    phone_buttons: Vec<Link>,
    phone_links: Vec<Link>,
    links: Vec<Link>,
    users: Vec<UserContext>,
    pagination: Vec<Link>,
    user: UserContext,
    redirect_message: Option<String>,
}

pub struct DUsersPage {
    conn: DbConn,
    current_page: u32,
    admin: User,
    redirect_message: Option<String>,
}

impl<'r> DUsersPage {
    fn new(
        conn: DbConn,
        current_page: u32,
        admin: User,
        redirect_message: Option<String>,
    ) -> Self {
        Self {
            conn,
            current_page,
            admin,
            redirect_message,
        }
    }

    fn get_context(&mut self) -> PageContext {
        // get links for the sidebar view
        let links = get_site_links();
        let phone_buttons: Vec<Link> = links.iter()
            .take(2)
            .map(|link| link.clone())
            .collect();
        let phone_links: Vec<Link> = links.iter()
            .skip(2)
            .map(|link| link.clone())
            .collect();

        PageContext {
            phone_buttons: phone_buttons,
            phone_links: phone_links,
            links: links,
            users: vec![],
            pagination: vec![],
            user: self.admin.clone().into(),
            redirect_message: self.redirect_message.clone(),
        }
    }

    fn index_handler(&mut self, req: &Request) -> response::Result<'r> {
        let mut context = self.get_context();

        context.users = self.get_users_for_current_page()
            .map_err(|_| Status::InternalServerError)?;

        // convert pagination from Vec<u32> into Vec<Link>
        let mut pagination = self.pagination_links("/dashboard/members/")
            .map_err(|_| Status::InternalServerError)?;
        // revers the numbers so it feels like arabic language RTL
        pagination.reverse();
        context.pagination = pagination;

        Template::render("dashboard/members", context).respond_to(req)
    }
}

impl<'r> Responder<'r> for DUsersPage {
    fn respond_to(mut self, req: &Request) -> response::Result<'r> {
        if !self.admin.is_admin() {
            return Err(Status::NotFound);
        }
        self.index_handler(req)
    }
}

impl UsersDb for DUsersPage { }

impl DatabaseConnection for DUsersPage {
    fn db_connection(&self) -> &DbConn {
        &self.conn
    }
}

impl Pagination for DUsersPage {
    fn items_per_page(&self) -> u32 {
        5
    }

    fn pagination_level(&self) -> u32 {
        6
    }

    fn all_items(&self) -> Result<u32> {
        self.get_all_users_number()
    }

    fn page(&self) -> u32 {
        self.current_page
    }

    fn set_page(&mut self, page: u32) {
        self.current_page = page;
    }
}

#[get("/dashboard/members/<page>")]
pub fn d_users(
    conn: DbConn,
    admin: User,
    page: u32,
    flash: Option<FlashMessage>
) -> DUsersPage {
    DUsersPage::new(
        conn,
        page,
        admin,
        flash.map(|msg| format!("{}: {}", msg.name(), msg.msg()))
    )
}

pub struct UsersDeleteUserPage {
    pub conn: DbConn,
    pub admin: User,
    pub page_id: PageId,
}

impl UsersDb for UsersDeleteUserPage { }
impl DatabaseConnection for UsersDeleteUserPage {
    fn db_connection(&self) -> &DbConn {
        &self.conn
    }
}

impl<'r> Responder<'r> for UsersDeleteUserPage {
    fn respond_to(mut self, req: &Request) -> response::Result<'r> {
        if !self.admin.is_admin() {
            return Err(Status::NotFound);
        }

        match self.delete_user(&self.page_id) {
            Ok(_) => {
                Flash::new(
                    Redirect::to("/dashboard/members/1"),
                    "تمت العملية",
                    format!("تم حذف العضو {} من النظام", self.page_id)
                ).respond_to(req)
            },
            Err(ref err) => {
                Flash::new(
                    Redirect::to("/dashboard/members/1"),
                    "لم تتم العملية",
                    format!("حصل خطأ أثناء حذف المستخدم {}، بسبب: {}", self.page_id, err)
                ).respond_to(req)
            },
        }
    }
}

#[get("/dashboard/members/delete/<page_id>")]
pub fn d_users_delete_user(
    conn: DbConn,
    admin: User,
    page_id: PageId,
) -> UsersDeleteUserPage {
    UsersDeleteUserPage {
        conn,
        admin,
        page_id,
    }
}

pub fn d_users_routes() -> Vec<Route> {
    routes![d_users, d_users_delete_user]
}
