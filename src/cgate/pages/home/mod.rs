use rocket_contrib::Template;
use contexts::Link;
use database::{
    DbConn,
    DatabaseConnection,
    users::{User, UserContext},
    services::{ServiceContext, ServicesDb},
    posts::{PostContext, PostsDb},
    clubs::{ClubContext, ClubsDb},
    courses::{CourseContext, CoursesDb},
};
use rocket::{
    Route,
    request::Request,
    response::{self, Responder},
    http::Status,
};

#[derive(Serialize)]
pub struct PageContext {
    phone_buttons: Vec<Link>,
    phone_links: Vec<Link>,
    links: Vec<Link>,
    services: Vec<ServiceContext>,
    posts: Vec<PostContext>,
    clubs: Vec<ClubContext>,
    courses: Vec<CourseContext>,
    user: Option<UserContext>
}

pub struct HomePage {
    conn: DbConn,
    user: Option<User>
}

impl<'r> HomePage {
    pub fn new(conn: DbConn, user: Option<User>) -> Self {
        Self {
            conn,
            user,
        }
    }

    fn get_context(&mut self) -> PageContext {
        let mut links = super::get_site_links();
        links[0].to_active();
        let phone_buttons: Vec<Link> = links.iter()
            .take(2)
            .map(|link| link.clone())
            .collect();
        let phone_links: Vec<Link> = links.iter()
            .skip(2)
            .map(|link| link.clone())
            .collect();

        PageContext {
            phone_buttons: phone_buttons,
            phone_links: phone_links,
            links: links,
            services: vec![],
            posts: vec![],
            clubs: vec![],
            courses: vec![],
            user: None,
        }
    }

    fn index_handler(&mut self, req: &Request) -> response::Result<'r> {
        let mut context = self.get_context();
        // fetch all services
        let services = self.get_services_context()
            .map_err(|_| Status::InternalServerError)?;
        // fetch last 5 posts
        let posts = self.get_last_posts_context(5)
            .map_err(|_| Status::InternalServerError)?;
        // fetch last 4 clubs
        let clubs = self.get_last_clubs_context(4)
            .map_err(|_| Status::InternalServerError)?;
        // fetch last 6 courses
        let courses = self.get_last_courses_context(6)
            .map_err(|_| Status::InternalServerError)?;

        context.services = services;
        context.posts = posts;
        context.clubs = clubs;
        context.courses = courses;

        // user info
        context.user = self.user.clone().map(|user| user.into());

        Template::render("home/index", context).respond_to(req)
    }
}

impl<'r> Responder<'r> for HomePage {
    fn respond_to(mut self, req: &Request) -> response::Result<'r> {
        self.index_handler(req)
    }
}

impl ServicesDb for HomePage { }
impl ClubsDb for HomePage { }
impl CoursesDb for HomePage { }
impl PostsDb for HomePage { }

impl DatabaseConnection for HomePage {
    fn db_connection(&self) -> &DbConn {
        &self.conn
    }
}

pub fn home_routes() -> Vec<Route> {
    routes![home]
}

#[get("/")]
pub fn home(conn: DbConn, user: Option<User>) -> HomePage {
    HomePage::new(conn, user)
}
