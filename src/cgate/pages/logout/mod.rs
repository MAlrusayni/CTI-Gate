use database::users::User;
use rocket::{
    Route,
    request::Request,
    response::{self, Responder, Redirect},
    http::{Cookies, Cookie},
};

pub struct LogoutPage<'c> {
    cookies: Cookies<'c>,
}

impl<'c, 'r> LogoutPage<'c> {
    pub fn new(cookies: Cookies<'c>) -> Self {
        Self {
            cookies
        }
    }

    fn index_handler(&mut self, req: &Request) -> response::Result<'r> {
        self.cookies.remove_private(Cookie::named("username"));
        self.cookies.remove_private(Cookie::named("password"));
        Redirect::to("/").respond_to(req)
    }
}

impl<'c, 'r> Responder<'r> for LogoutPage<'c> {
    fn respond_to(mut self, req: &Request) -> response::Result<'r> {
        self.index_handler(req)
    }
}

pub fn logout_routes() -> Vec<Route> {
    routes![logout, logout_while_not_loged_in]
}

#[get("/logout")]
pub fn logout<'c>(_user: Option<User>, cookies: Cookies<'c>) -> LogoutPage<'c> {
    LogoutPage::new(cookies)
}

#[get("/logout", rank = 2)]
pub fn logout_while_not_loged_in() -> Redirect {
    Redirect::to("/")
}
