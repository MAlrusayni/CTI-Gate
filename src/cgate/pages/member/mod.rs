use rocket_contrib::Template;
use contexts::Link;
use database::{
    DbConn,
    DatabaseConnection,
    types::PageId,
    users::{User, UserContext, UsersDb},
    posts::{PostContext, PostsDb},
    clubs::ClubContext,
};
use rocket::{
    Route,
    request::Request,
    response::{self, Responder},
    http::Status,
};

#[derive(Serialize)]
pub struct PageContext {
    phone_buttons: Vec<Link>,
    phone_links: Vec<Link>,
    links: Vec<Link>,
    member: Option<UserContext>,
    posts_number: u32,
    last_posts: Vec<PostContext>,
    joined_clubs: Vec<ClubContext>,
    user: Option<UserContext>,
}

pub struct MemberPage {
    conn: DbConn,
    member_page_id: PageId,
    user: Option<User>
}

impl<'r> MemberPage {
    pub fn new(conn: DbConn, member_page_id: PageId, user: Option<User>) -> Self {
        Self {
            conn,
            member_page_id,
            user,
        }
    }

    fn get_context(&mut self) -> PageContext {
        let mut links = super::get_site_links();
        links[0].to_active();
        let phone_buttons: Vec<Link> = links.iter()
            .take(2)
            .map(|link| link.clone())
            .collect();
        let phone_links: Vec<Link> = links.iter()
            .skip(2)
            .map(|link| link.clone())
            .collect();

        PageContext {
            phone_buttons: phone_buttons,
            phone_links: phone_links,
            links: links,
            member: None,
            posts_number: 0,
            last_posts: vec![],
            joined_clubs: vec![],
            user: None,
        }
    }

    fn index_handler(&mut self, req: &Request) -> response::Result<'r> {
        let mut context = self.get_context();

        // user info
        context.user = self.user.clone().map(|user| user.into());

        // get member by page id
        let user = self.get_user_by_page_id(&self.member_page_id)
            .map_err(|_| Status::NotFound)?;

        // user last 10 posts
        context.last_posts = self.get_last_posts_context_by_auther(&user.id, 20)
            .map_err(|_| Status::InternalServerError)?;

        // TODO: find clubs this member are part of it

        // TODO: posts number
        context.posts_number = self.get_posts_number_written_by_auther(&user.id)
            .map_err(|_| Status::InternalServerError)?;

        context.member = Some(user.into());

        Template::render("member/index", context).respond_to(req)
    }
}

impl<'r> Responder<'r> for MemberPage {
    fn respond_to(mut self, req: &Request) -> response::Result<'r> {
        self.index_handler(req)
    }
}

impl UsersDb for MemberPage { }
impl PostsDb for MemberPage { }

impl DatabaseConnection for MemberPage {
    fn db_connection(&self) -> &DbConn {
        &self.conn
    }
}

pub fn member_routes() -> Vec<Route> {
    routes![member]
}

#[get("/member/<member_page_id>")]
pub fn member(
    conn: DbConn,
    member_page_id: PageId,
    user: Option<User>
) -> MemberPage {
    MemberPage::new(conn, member_page_id, user)
}
