use contexts::{Link, Icon};
use errors::*;

pub mod home;
pub mod news;
pub mod clubs;
pub mod courses;
pub mod contact_us;
pub mod about_us;
pub mod new_member;
pub mod login;
pub mod logout;
pub mod member;
pub mod dashboard;

pub fn get_site_links() -> Vec<Link> {
    vec![
        Link::normal("الرئيسية", Some(Icon::new("fas", "home")), "/"),
        Link::normal("الأخبار", Some(Icon::new("fas", "newspaper")), "/news/"),
        Link::normal("النوادي", Some(Icon::new("fas", "users")), "/clubs/"),
        Link::normal("الدورات", Some(Icon::new("fas", "university")), "/courses/"),
        Link::normal("عن المركز", Some(Icon::new("fas", "info")), "/about-us/"),
        Link::normal("تواصل معنى", Some(Icon::new("fas", "at")), "/contact-us/")
    ]
}

pub trait Pagination {
    // return the numbers of items that will be displayed in single page
    fn items_per_page(&self) -> u32;
    // return the pagination level, e.g. when you in page number 10, and this function reutrn 6
    // as the level of the pagination, then the pagination will be [8, 9, 10, 11, 12, 13]
    fn pagination_level(&self) -> u32;
    // returns number of items that will be displayed in this page
    fn all_items(&self) -> Result<u32>;
    // get the current page number
    fn page(&self) -> u32;
    // set the current page number
    fn set_page(&mut self, page: u32);

    // computes the how many page needed to contain all items where each page will contain item_per_page
    fn pages_number(&self) -> Result<u32> {
        Ok((self.all_items()? as f32 / self.items_per_page() as f32).ceil() as u32)
    }

    fn is_vaild_page_number(&self, page: u32) -> Result<()> {
        // check if page within available pages that can be generated
        Self::in_range(1, self.pages_number()?, page)
    }

    // chech if page number in range of min and max (pagination)
    fn in_range(min: u32, max: u32, page: u32) -> Result<()> {
        if page > max || page < min {
            Err(ErrorKind::OutOfPaginationRange(min, max, page).into())
        } else {
            Ok(())
        }
    }

    // computes the pagination and return it
    fn pagination(&self) -> Result<Vec<u32>> {
        let page = self.page();
        let pages_number = self.pages_number()?;
        Self::in_range(1, pages_number, page)?;

        let mut pagination = vec![];

        let half = self.pagination_level() as f32 / 2 as f32;
        let left = page as i32 - half.floor() as i32;
        let right = page as i32 + half.ceil() as i32;

        for n in left..=right {
            if n >= 1 && n <= pages_number as i32 {
                pagination.push(n as u32)
            }
        }

        Ok(pagination)
    }

    // returns Vec<Link> insted of Vec<u32>, useful for Context structs
    fn pagination_links(&self, url: &str) -> Result<Vec<Link>> {
        let page = self.page();
        self.is_vaild_page_number(page)?;
        let pagination = self.pagination()?;
        let pagination = pagination.into_iter()
            .map(|number| {
                let url = format!("{}{}", url, number);
                let mut link = Link::normal(format!("{}", number), None, url);
                if number == page {
                    link.to_active();
                }
                link
            })
            .collect();
        Ok(pagination)
    }
}
