use errors::*;
use rocket_contrib::Template;
use rocket::{
    Route,
    request::{Request, Form},
    response::{self, Redirect, Responder},
};
use contexts::Link;
use database::{
    DbConn,
    DatabaseConnection,
    users::{
        User,
        UsersDb,
        types::{Username, FirstName, LastName, Phone, Email, Password},
    },
};

#[derive(FromForm, Debug)]
pub struct SignUpForm {
    pub username: Result<Username>,
    pub first_name: Result<FirstName>,
    pub last_name: Result<LastName>,
    pub phone: Option<Result<Phone>>,
    pub email: Result<Email>,
    pub password: Result<Password>,
}

impl SignUpForm {
    pub fn ok_or_incomplete_form<T: Clone>(value: &Result<T>) -> Result<T> {
        match value {
            Ok(ref value) => Ok(value.clone()),
            Err(ref err) => {
                let reason = format!("{}", err);
                Err(ErrorKind::IncompleteSignUpForm(reason))?
            },
        }
    }
}

#[derive(Debug, Serialize)]
pub struct PageContext {
    phone_buttons: Vec<Link>,
    phone_links: Vec<Link>,
    links: Vec<Link>,
    show_success_message: bool,
    other_err: Option<String>,
    username_err: Option<String>,
    first_name_err: Option<String>,
    last_name_err: Option<String>,
    phone_err: Option<String>,
    email_err: Option<String>,
    password_err: Option<String>,
}

impl PageContext {
    pub fn is_sign_up_form_invalid(&self) -> bool {
        self.username_err.is_some() || self.first_name_err.is_some() ||
            self.last_name_err.is_some() || self.phone_err.is_some() ||
            self.email_err.is_some() || self.password_err.is_some()
    }

    fn find_sign_up_form_errors(&mut self, sign_up: &SignUpForm) {
        let to_str = |err: &Error| -> Option<String> {
            Some(format!("{}", err))
        };

        if let Err(ref err) = sign_up.username { self.username_err = to_str(err) }
        if let Err(ref err) = sign_up.first_name { self.first_name_err = to_str(err) }
        if let Err(ref err) = sign_up.last_name { self.last_name_err = to_str(err) }
        if let Err(ref err) = sign_up.email { self.email_err = to_str(err) }
        if let Err(ref err) = sign_up.password { self.password_err = to_str(err) }

        if let Some(ref phone) = sign_up.phone {
            if let Err(ref err) = phone {
                self.phone_err = to_str(err)
            }
        }
    }
}

pub struct NewMemberPage {
    conn: DbConn,
    sign_up_form: Option<SignUpForm>,
}

impl<'r> NewMemberPage {
    fn new(conn: DbConn, sign_up_form: Option<SignUpForm>) -> Self {
        Self {
            conn,
            sign_up_form,
        }
    }

    fn get_context(&mut self) -> PageContext {
        // get links for the sidebar view
        let mut links = super::get_site_links();
        links[5].to_active();
        let phone_buttons: Vec<Link> = links.iter()
            .take(2)
            .map(|link| link.clone())
            .collect();
        let phone_links: Vec<Link> = links.iter()
            .skip(2)
            .map(|link| link.clone())
            .collect();

        let context = PageContext {
            phone_buttons: phone_buttons,
            phone_links: phone_links,
            links: links,
            show_success_message: false,
            other_err: None,
            username_err: None,
            first_name_err: None,
            last_name_err: None,
            phone_err: None,
            email_err: None,
            password_err: None,
        };

        context
    }

    fn index_handler(&mut self, req: &Request) -> response::Result<'r> {
        let context = self.get_context();
        let template = Template::render("new_member/index", context);
        template.respond_to(req)
    }

    fn sign_up_handler(&mut self, req: &Request) -> response::Result<'r> {
        if self.sign_up_form.is_none() {
            // redirecting to index_handler() if there was no signup form
            return Redirect::to("/new-membe").respond_to(req);
        }

        let mut context = self.get_context();
        // unwrap is fine here, since we already checked for none
        let sign_up = self.sign_up_form.as_ref().unwrap();

        // check if sign_up form fields have any error, return with error messages
        // if so.
        context.find_sign_up_form_errors(sign_up);
        if context.is_sign_up_form_invalid() {
            let template = Template::render("new_member/index", context);
            return template.respond_to(req);
        }

        match self.add_new_user(sign_up) {
            Ok(_) => {
                context.show_success_message = true;
            },
            Err(Error(ErrorKind::NotAvailableUsername(_), _)) => {
                context.other_err = Some("اسم المستخدم مستعمل، قم باختيار اسم آخر".to_string());
            },
            Err(Error(ErrorKind::NotAvailablePageId(_), _)) => {
                context.other_err = Some("حصلة مشكلة أثناء إنشاء صفحة للعضوية الجديدة، تغيير الاسم قد يحل المشكلة".to_string());
            },
            Err(Error(ErrorKind::NotAvailablePhone(_), _)) => {
                context.other_err = Some("الهاتف مستعمل، قم بإدخال هاتف آخر".to_string());
            },
            Err(Error(ErrorKind::NotAvailableEmail(_), _)) => {
                context.other_err = Some("البريد الإلكتروني مستعمل، قم بإدخال بريد آخر من فضلك".to_string());
            },
            _ => {
                context.other_err = Some("حصلة مشكلة أثناء إنشاء عضويتك، راجع الإدارة إذا تكررت هذه المشكلة".to_string());
            },
        };

        let template = Template::render("new_member/index", context);
        template.respond_to(req)
    }
}

impl<'r> Responder<'r> for NewMemberPage {
    fn respond_to(mut self, req: &Request) -> response::Result<'r> {
        if self.sign_up_form.is_none() {
            return self.index_handler(req)
        }
        self.sign_up_handler(req)
    }
}

impl UsersDb for NewMemberPage { }

impl DatabaseConnection for NewMemberPage {
    fn db_connection(&self) -> &DbConn {
        &self.conn
    }
}

pub fn signup_routes() -> Vec<Route> {
    routes![
        signup_while_loged_in,
        signup,
        signup_form_while_loged_in,
        signup_form
    ]
}

#[get("/new-member")]
pub fn signup_while_loged_in(_user_guard: User) -> Redirect {
    Redirect::to("/")
}

#[get("/new-member", rank = 2)]
pub fn signup(conn: DbConn) -> NewMemberPage {
    NewMemberPage::new(conn, None)
}

#[post("/new-member", data = "<_message>")]
pub fn signup_form_while_loged_in(
    _user_guard: User,
    _message: Option<Form<SignUpForm>>
) -> Redirect {
    Redirect::to("/")
}

#[post("/new-member", data = "<message>", rank = 2)]
pub fn signup_form(conn: DbConn, message: Option<Form<SignUpForm>>) -> NewMemberPage {
    let message = message.map(|msg| msg.into_inner());
    NewMemberPage::new(conn, message)
}
