use errors::*;
use rocket_contrib::Template;
use contexts::Link;
use pages::Pagination;
use database::{
    DatabaseConnection,
    DbConn,
    types::PageId,
    users::{User, UserContext},
    posts::{PostsDb, PostContext},
};
use rocket::{
    Route,
    request::Request,
    response::{self, Responder},
    http::Status,
};

#[derive(Serialize)]
pub struct PageContext {
    phone_buttons: Vec<Link>,
    phone_links: Vec<Link>,
    links: Vec<Link>,
    posts: Vec<PostContext>,
    pagination: Vec<Link>,
    user: Option<UserContext>,
}

pub struct NewsPage {
    conn: DbConn,
    current_page: u32,
    user: Option<User>,
}

impl<'r> NewsPage {
    pub fn new(conn: DbConn, current_page: u32, user: Option<User>) -> Self {
        Self {
            conn,
            current_page,
            user,
        }
    }

    fn get_context(&mut self) -> PageContext {
        // get links for the sidebar view
        let mut links = super::get_site_links();
        links[1].to_active();
        let phone_buttons: Vec<Link> = links.iter()
            .take(2)
            .map(|link| link.clone())
            .collect();
        let phone_links: Vec<Link> = links.iter()
            .skip(2)
            .map(|link| link.clone())
            .collect();

        PageContext {
            phone_buttons: phone_buttons,
            phone_links: phone_links,
            links: links,
            posts: vec![],
            pagination: vec![],
            user: None,
        }
    }

    fn index_handler(&mut self, req: &Request) -> response::Result<'r> {
        let mut context = self.get_context();
        // fetch posts from database and convert them into Vec<PostContext>
        let posts = self.get_posts_context()
            .map_err(|_| Status::InternalServerError)?;

        // convert pagination from Vec<u32> into Vec<Link>
        let mut pagination = self.pagination_links("/news/page/")
            .map_err(|_| Status::InternalServerError)?;
        // revers the numbers so it feels like arabic language RTL
        pagination.reverse();

        // user info
        context.user = self.user.clone().map(|user| user.into());

        context.posts = posts;
        context.pagination = pagination;

        Template::render("news/index", context).respond_to(req)
    }
}

impl<'r> Responder<'r> for NewsPage {
    fn respond_to(mut self, req: &Request) -> response::Result<'r> {
        self.index_handler(req)
    }
}

impl DatabaseConnection for NewsPage {
    fn db_connection(&self) -> &DbConn {
        &self.conn
    }
}

impl Pagination for NewsPage {
    fn items_per_page(&self) -> u32 {
        5
    }

    fn pagination_level(&self) -> u32 {
        6
    }

    fn all_items(&self) -> Result<u32> {
        self.get_posts_number()
    }

    fn page(&self) -> u32 {
        self.current_page
    }

    fn set_page(&mut self, page: u32) {
        self.current_page = page;
    }
}

impl PostsDb for NewsPage { }

#[derive(Serialize)]
pub struct PostPageContext {
    phone_buttons: Vec<Link>,
    phone_links: Vec<Link>,
    links: Vec<Link>,
    post: Option<PostContext>,
    user: Option<UserContext>,
}

pub struct PostPage {
    conn: DbConn,
    post_page_id: PageId,
    user: Option<User>,
}

impl<'r> PostPage {
    pub fn new(conn: DbConn, post_page_id: PageId, user: Option<User>) -> Self {
        Self {
            conn,
            post_page_id,
            user,
        }
    }

    fn get_context(&mut self) -> PostPageContext {
        // get links for the sidebar view
        let mut links = super::get_site_links();
        links[1].to_active();
        let phone_buttons: Vec<Link> = links.iter()
            .take(2)
            .map(|link| link.clone())
            .collect();
        let phone_links: Vec<Link> = links.iter()
            .skip(2)
            .map(|link| link.clone())
            .collect();

         PostPageContext {
            phone_buttons: phone_buttons,
            phone_links: phone_links,
            links: links,
            post: None,
            user: None,
        }
    }

    fn index_handler(&mut self, req: &Request) -> response::Result<'r> {
        let mut context = self.get_context();
        // fetch posts from database and convert them into Vec<PostContext>
        context.post = Some(
            self.get_post_context_by_page_id(&self.post_page_id)
                .map_err(|_| Status::NotFound)?
        );

        // user info
        context.user = self.user.clone().map(|user| user.into());

        Template::render("news/post", context).respond_to(req)
    }
}

impl<'r> Responder<'r> for PostPage {
    fn respond_to(mut self, req: &Request) -> response::Result<'r> {
        self.index_handler(req)
    }
}

impl DatabaseConnection for PostPage {
    fn db_connection(&self) -> &DbConn {
        &self.conn
    }
}

impl PostsDb for PostPage { }


pub fn news_routes() -> Vec<Route> {
    routes![news, news_page_number, post]
}

#[get("/news")]
pub fn news(conn: DbConn, user: Option<User>) -> NewsPage {
    NewsPage::new(conn, 1, user)
}

#[get("/news/page/<page>")]
pub fn news_page_number(conn: DbConn, page: u32, user: Option<User>) -> NewsPage {
    NewsPage::new(conn, page, user)
}

// TODO: let every post have its own page
#[get("/post/<post_id>")]
pub fn post(
    conn: DbConn,
    post_id: PageId,
    user: Option<User>
) -> PostPage {
    PostPage::new(conn, post_id, user)
}
