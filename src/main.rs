extern crate cgate;

fn main() {
    if let Err(ref error) = cgate::run() {
        use std::io::Write;
        let stderr = &mut ::std::io::stderr();
        let errmsg = "Error wrting to stderr";

        writeln!(stderr, "error: {}", error).expect(errmsg);

        for error in error.iter().skip(1) {
            writeln!(stderr, "caused by: {}", error).expect(errmsg);
        }

        if let Some(backtrace) = error.backtrace() {
            writeln!(stderr, "backtrace: {:?}", backtrace).expect(errmsg);
        }

        ::std::process::exit(1);
    }
}
